var dash = require('appmetrics-dash');
require('appmetrics-dash').monitor();
const config = require('./config');
const db = require('./db/mongo');
const compression = require('compression')
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const socket = require('./api/socket/socket');

socket.init(server);

dash.monitor({server: server});
app.use(compression());
const rateLimit = require('express-rate-limit');

//SECURITY
const helmet = require('helmet');
app.use(helmet());

app.enable("trust proxy");

if (config.security.limitPer15Mins != 0){
    var limiter = new rateLimit({
        windowMs: 15*60*1000,
        max: config.security.limitPer15Mins,
        delayMs: 0
    });
    app.use(limiter);
};
//SECURITY

const jwt = require('./api/auth/auth.jwt');
const bodyParser = require('body-parser');
const cors = require('cors');

const authController = require('./api/auth/auth.controller');
const userController = require('./api/user/user.controller');
const deviceController = require('./api/device/device.controller');
const tsController = require('./api/telemetry/ts.controller');
const hashController = require('./api/device/hash/device.hash.controller');
const andrejController = require('./api/andrej/andrej.controller');
const deviceAuthController = require('./api/device/auth/device.auth.controller')
const deviceTokenController = require('./api/device/token/device.token.controller');
const externalApiController = require('./api/API/api.controller')

app.use(function(err, req, res, next) {
    console.error(err);
    if (res.headersSent) {
        return next(err)
    }
    res.status(500).send("Unknown error")
});

app.use(cors());
app.options('*', cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(jwt.passport.initialize());
app.use(jwt.passport.session());

app.use('/auth', authController);
app.use('/user', userController);
app.use('/device', deviceController);
app.use('/device/auth', deviceAuthController);
app.use('/ts', tsController);
app.use('/admin/hash', hashController);
app.use('/a', andrejController);

//API access
/*
const limiterAPI = rateLimit({
    windowMs: 1 * 30 * 1000, // 15 minutes
    max: 2 // limit each IP to 100 requests per windowMs
});
app.use('/device/token', deviceTokenController);
app.use("/api/", limiterAPI);
app.use('/api/v1/telemetry', externalApiController);
*/

app.get('/test_jwt', jwt.passport.authenticate('jwt'), function (req, res) {
    res.json({success: 'You are authenticated with JWT!', user: req.user});
})

app.get('/test_jwt_admin', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']), function (req, res) {
    res.json({success: 'You are authenticated with JWT!', user: req.user});
})

/*process.on('unhandledRejection', function (reason, p) {
    console.error(new Date().toLocaleString(),'Caught rejection: ' + reason, p);
});

process.on('uncaughtException', function() {
    console.error(new Date().toLocaleString(),'Caught exception ');
    process.exit(1);
});*/

app.use(function (req, res, next) {
    res.status(404).send("Content not found")
})

console.log(new Date().toLocaleString(),"Listening on port " + config.server.port);
server.listen(config.server.port || 8999);
