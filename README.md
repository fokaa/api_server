# weather server

*Configuration*
All configurable settings are located in config.json. Server requires mongodb, influxdb and smtp servers. 
Admin account will be created at first run. If no jwt secret is provided, server will use its own dumb secret code.

v1.3
## Whats new:
* SMTP configuration
* New APIs on Andrej's request under uri /a
* DB schemas enhancements 

v1.2
## Whats new:
* Hashtable for devices - a device can be registered ONLY if it exists in hashtable and is not already registered, timeseries module also actively uses this db
* REST call for device hash table creation (admin only)
* Enhanced devices schema and return values from devices controller
* Enhanced timeseries calls, now they all accept timestamp in s, ms and ns
* Enhanced last values timeseries call, now it displays timestamp along the values


## APIs

if login or refresh token si required, the server returns
*401 Unauthorized*

if user is not authorized for requested content, the server returns
*401 Not authorized to access this content*

**/a**  (a ako Andrej)
*       /profile                GET (get your own profile with favorites)*
        returns:
        200 {
                "_id":"",
                "email":"",
                "username":"",
                "favorites":[{
                        "id":"device id",
                        "devicename":"device name",
                        "username":"owner"}
                        ]
                "role":"",
                "subscription":[],                      
             }
        404 User not found
        500 Unknown error
        
*       /profile/favorites          GET (get your own favorites)
        /profile/favorites/:id      POST (add favorite)
        /profile/favorites/:id      DELETE (remove favorite)
        returns:
        200 {
                "favorites":[{
                        "id":"device id",
                        "devicename":"device name",
                        "username":"owner"}
            ]}
        404 User not found
        500 Unknown error
        
*       /devices                GET (get ALL accessible devices)
        /devices/owned          GET (get owned devices)
        /devices/shared         GET (get devices shared with users)
        returns:
        200 [{
                        "shared":true,
                        "_id":"device id",
                        "sn":"device number given to customer for registration",
                        "type":"basic",
                        "owner":{"username":"user name"},
                        "provider":"sigfox",
                        "deviceId":"device id sent by network f.e. bl6-369A79",
                        "name":"name given to device by owner"
             }]
        404 Not found
        500 Unknown error

*       /overview/?sensors=temperature+humidity         GET (last values of listed sensors and device info for ALL accessible devices)
        /overview/owned/?sensors=temperature+humidity   GET (last values of listed sensors and device info for owned devices)
        /overview/shared/?sensors=temperature+humidity  GET (last values of listed sensors and device info for devices shared with users)
        returns:
        200 [{
                "shared":true,
                "_id":"device id",
                "sn":"device number given to customer for registration. devices hashtable holds values sn<=>deviceId ",
                "type":"basic",
                "owner":{"username":"user name"},
                "provider":"sigfox",
                "deviceId":"device id sent by network f.e. bl6-369A79",
                "name":"name given to device by owner"
                "last":{       (if no sensor selected all existing sensor values will be returned)
                              "time":1526500800000,
                              "battery":3.8,
                              "dewPoint":285.1,
                              "frostPoint":200,
                              "humidity":53.6,
                              "irradiation":0,
                              "pressure":97664,
                              "rain":0,
                              "seqNumber":"2879",
                              "snr":33.65,
                              "soil":0,
                              "temperature":294.9
                      }
            },...]


**/auth**
*     /login         POST*
        {
            "email":"",
            "password":""
        }
        returns:
        200
            {
                "token":"",
                "refreshToken":""
            }
        401 'Not logged in'
    
*       /change         POST (with jwt header, changes password)*
        {
            "oldPassword":"",
            "newPassword":""
        }
        returns:
        200 Password changed
        401 Old password not submitted
        401 New password not submitted
        401 Passwords identical
        401 Weak password
        401 Password don't match
        500 Unknown error
        
*       /refresh         POST (request new token using refresh token)*
         {
             "refreshToken":""
         }   
         returns:
             200
             {
                 "token":""
             }
             500 Bad token
             
*       /register         POST        (registration code will be shown in console)*
        {
            "email":"",
            "username":"",
            "password ":""
        }
        returns:
           200 User reistered, email dispatched
           400 User already exists
           400 Activation mail already sent
           400 Cannot register user
           401 Weak password
           401 Email address not valid
           500 Unknown error         
    
*       /activate/:id       GET     (:id is registration code)*
        returns:
            200 User activated
            400 Link expired
            404 Link not valid
            404 User not found
            500 Unknown error
            
*       /resend        POST (resends the activation link)*
            {
                "email":""
            }
            returns:
                200 Link resent
                400 Link expired
                400 User already activated
                404 Link not valid
                404 Email not recognized
                500 Unknown error
                
*       /reset        POST (ask to reset password)*
            {
                "email":""
            }
            returns:
                200 Reset code sent
                400 User not activated
                404 User not found
                500 Unknown error
                
*     /reset        POST (resets password)        *
            {
                "mail":"",
                "resetcode":"",
                "password":""
            }
            returns:
                200 Password changed
                400 Code expired
                401 Weak password
                404 Wrong reset code
                404 User not found
                500 Unknown error
                
**/user**

*       /        GET (returns list of users in case of admin and info about user itself in case of user)*
        returns:
            200 {
                        "role":"user",
                        "subscription":[],
                        "_id":"","email":"",
                        "username":""
                    }
                    or
                    [{
                        "role":"",
                        "disabled":true/false,
                        "activated":true/false,
                        "subscription":[],
                        "_id":"",
                        "email":"",
                        "username":"",
                        }]
            404 No data
            500 Unknown error
            
*       /:id        GET (get user info)*
        returns:
            200  {
                        "role":"user",
                        "subscription":[],
                        "_id":"","email":"",
                        "username":""
                    }
            404 User not found
            500 Unknown error
  
*       /:id        DELETE *        *(admin only)*
        returns:
            200 User deleted
            404 User not found
            500 Unknown error
    
*       /             POST            (create user in DB role=user, admin only)  *  
        {
            "username":"",
            "email":"",
            "password":""
        }
        returns:
            200 User created
            400 Bad data
            401 User already exists
            500 Unknown error
   
*       /            PUT            (modifies user in DB, admin only, only one of the  parameters required)*
        {
            "username":"",
            "role":""
        }
        returns:
            200 User modified
            404 User not found
            500 Unknown error
            
*       /:id/disable        GET (disables user, admin only)*
*       /:id/enable         GET (disables user, admin only)*
        returns:
            200 User disabled/User enabled
            404 User not found
            500  Unknown error
            
**/device**
    
*       /            GET         (returns either user owned devices or all devices in case of admin)*
        returns:
            200 [
            {
                            "shared": true/false,
                            "disabled": true/false,
                            "tags": ["",""],
                            "comments":[],
                             "_id":"", 
                             "sn":"",
                             "type":"",
                             "owner":"",
                             "position":{"type":"Point","coordinates":[,]},
                             "name":""
                             }]
                             
*       /            POST        (register new device)*
         {
                "sn":"", (required)
                "tags":[],
                "comments":[],
                 "position":[,],
                 "name":"" (if name is empty, will be equal to sn)
         }
         returns:
             200 Device registered
             400 Not saved
             401 Device already registered
             404 Device not found
             500 Unknown error
             
*       /:id            PUT        (modify device,  owner or admin only, *only one of the  parameters required*)*
        {
         "type":"",
         "name":"",
         "position":[,]
         "shared":true/false
         "tags":[]
        }
        returns:
         200 Device modified
         401 Not authorized
         404 Device not found
         500 Unknown error
         
 *      /:id            DELETE        (delete device, admin only)*
        returns:
         200 Device deleted
         401 Not authorized
         404 Device not found
         500 Unknown error
         
*       /:id/disable    GET     (owner or admin only)*
*       /:id/enable     GET     (owner or admin only)*
        returns:
         200 Device enabled/ Device disabled
         401 Not authorized
         404 Device not found
         500 Unknown error
         
*       /:id/assign     POST     (assign device to user, admin only, only one parameter required, if both provided, id will be used)*
        {
         "id":"",
         "email":""
        }
        returns:
         200 Device assigned
         404 User not found
         404 Device not found
         500 Unknown error
         
**/ts**

*       /:id?from=&to=&sensors=            GET         (returns TABLE of device measurements with sn == id! NOT id used by sigfox!)*
        parameters - all are optional (to, from, agg have no effect with "last" fn)
            from, to - seconds/milliseconds/nanoseconds from and to
            sensors - sensor measurements (f.g. sensors=temperature+co2+battery)
        returns:
            200 [{
                time:....,
                meas1:...,
                meas2:....
            }]
            401 Not authorized to access device
            404 Device not found
            500 Unknown error

*       /:id/fn?from=&to=&agg=&sensors=            GET         (returns AGGREGATED TIMESERIES data for device with sn == id! NOT id used by sigfox!)*
        fn - can be min, max, mean, count, distinct, median, mode, spread ,stddev , sum, integral
             https://docs.influxdata.com/influxdb/v1.5/query_language/functions/
        parameters - all are optional (to, from, agg have no effect with "last" fn)
            from, to - seconds/milliseconds/nanoseconds from and to
            agg - aggregation time, can be number + one of s, m, h, d, w, M, Y (f.g. agg=34m - 34 minutes) 
            sensors - sensor measurements (f.g. sensors=temperature+co2+battery)
        returns:
            200 [{
                time:....,
                meas1:...,
                meas2:....
            }]
            401 Not authorized to access device
            404 Device not found
            500 Unknown error

**/admin/hash**

*       /        POST (create unique deviceId/provider/serial number hash in db, admin only)*
        {        (all values are required)
            sn: ,       (number given to customer - f.e. barcode)
            device: ,   (device id sent by provider)
            provider: , (provider indetification sent by provider)
            type: ,     (type of device: for now basic/extended)
        }
        returns:
            200 Hash created
            400 Duplicate hash
            400 Bad data
            590 Unknown error
