
var robots = function(req, res, next) {
    if ((typeof req.headers["host"] !== "undefined" 
        && req.headers["host"].indexOf('corryn.net') === -1) &&
        (typeof req.headers["referer"] === "undefined" ||
        typeof req.headers["origin"] === "undefined" ||
        req.headers["referer"].indexOf("https://weather.allmeteo.com") === -1 ||
        req.headers["origin"].indexOf("https://weather.allmeteo.com") === -1)) {
        req.body.robot = true;
        if (typeof req.headers['x-forwarded-for'] !== "undefined") {
            req.body.robotAddress = req.headers['x-forwarded-for'];
        } else {
            req.body.robotAddress = 'unknown';
        }
    }
    next();
}

var middlewares = {
    "robots": robots
}

module.exports = middlewares;
