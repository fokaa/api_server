var mongoose = require('mongoose');

var ActivateSchema = new mongoose.Schema({
    link: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    expiration: {
        type: Date,
        default: (Date.now() + 86400000)
    }
});

ActivateSchema.set("timestamps", true);

var acctivate = mongoose.model('Activation', ActivateSchema);

module.exports = mongoose.model('Activation');
