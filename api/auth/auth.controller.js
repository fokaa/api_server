const router = require('express').Router();

const jwt = require('./auth.jwt');
const authAPI = require('./auth.api');
const utils = require('../utils/helpers');

const bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(jwt.passport.initialize());
router.use(jwt.passport.session());

router.post('/login', function (req, res, next) {
    //console.log(req.headers);
    //console.log(req.connection);
    //console.log(new Date().toLocaleString(), req.body);
    if (typeof req.headers["referer"] === "undefined" ||
        typeof req.headers["origin"] === "undefined" ||
        req.headers["referer"].indexOf("https://weather.allmeteo.com") === -1 ||
        req.headers["origin"].indexOf("https://weather.allmeteo.com") === -1) {
        req.body.robot = true;
        req.body.robotAddress = req.headers['x-forwarded-for'];
	    if (req.body.email !== "jan.barani@baranidesign.com"){
            console.log(new Date().toLocaleString(),"ROBOT: ", req.body.email, req.headers['x-forwarded-for']);
        }
    }
    if (req.body.email == "demo@corryn.net"){
        console.log(new Date().toLocaleString(),"DEMO: ", req.body.email, req.headers['x-forwarded-for']);
    }
    var user = jwt.login(req.body).then(
        function(result){
            if (result.token){
                res.status(200).json(result);
            } else {
                res.status(401).send('Not logged in');
            }
        }
    );
});

router.all('/logout', jwt.passport.authenticate('jwt'), function (req, res, next) {
    jwt.logout(req.user._id);
    res.status(200).send('Logged out');
});

router.post('/change', jwt.passport.authenticate('jwt'), function (req, res, next) {
    if (!req.body.oldPassword){
        res.status(401).send("Old password not submitted");
    } else if (!req.body.newPassword){
        res.status(401).send("New password not submitted");
    } else if (req.body.oldPassword == req.body.newPassword) {
        res.status(401).send("Passwords identical");
    } else if (!utils.checkPasswordQuality(req.body.newPassword)){
        res.status(401).send("Weak password");
    }
    else {
        var pass = {};
        pass.email = req.user.email;
        pass.old = req.body.oldPassword;
        pass.new = req.body.newPassword;
        authAPI.newPassword(pass, function(result){
            if (result == 3){
                res.status(200).send("Password changed");
            } else if (result == 2) {
                res.status(401).send("Password don't match");
            } else {
                res.status(500).send("Unknown error");
            }
        })
    }
});

router.post('/refresh', function (req, res, next) {
    jwt.refresh(req.body.refreshToken, function(reason, token){
        if (reason == 2){
            res.status(200).json({"token": token});
        } else {
	        res.status(500).json("Bad token");
        }
    });
});

router.post('/token/reject', function (req, res, next) {
    if (jwt.reject(req.body.refreshToken)){
        res.send(204);
    }
    res.send(404);
});

router.post('/register', function(req, res, next){
    //TODO: Send activation link
    console.log("New registration ", req.body)
    if (!utils.checkPasswordQuality(req.body.password)){
        res.status(401).send("Weak password");
    } else if (!utils.checkEmail(req.body.email)){
        res.status(401).send("Email address not valid");
    } else {
        authAPI.register(req.body, function (data) {
            var code = 500;
            switch (data) {
                case 2:
                    message = "User already exists";
                    code = 400;
                    break;
                case 3:
                    message = "Activation mail already sent";
                    code = 400;
                    break;
                case 1:
                    message = "Unknown error";
                    code = 500;
                    break;
                case 4:
                    message = "Registration email dispatched";
                    code = 200;
                    break;
                default:
                    code = 400;
                    message = "Cannot register user";
            }
            res.status(code).json(message);
        });
    }
})

router.all('/activate/:id', function(req, res, next){
    authAPI.activate(req.params['id'], function(reason){
        var html1 = '<!DOCTYPE html>\
                            <html lang="en">\
                            <head>\
                                <meta charset="UTF-8">\
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">\
                                <meta http-equiv="X-UA-Compatible" content="ie=edge">\
                                <meta http-equiv="Refresh" content="3; url=https://weather.allmeteo.com">\
                                <title>User activation</title>\
                            </head>\
                            <body>';
                            
        var html2 = '</body></html>';
                            
        switch(reason){
            case 1:
                var html = html1 + "<br><br><center>Invalid reset link</center>" + html2;
                res.status(404).send(html);
                break;
            case 2:
                var html = html1 + "<br><br><center>Link expired</center>" + html2;
                res.status(400).send(html);
                break;
            case 3:
            var html = html1 + "<br><br><center>User not found</center>" + html2;
                res.status(404).send(html);
                break;
            case 4:
                var html = html1 + "<br><br><center>User activated successfully<br>You may now login</center>" + html2;
                res.status(200).send(html);
                break;
            default:
                var html = html1 + "<br><br><center>Unknown error</center>" + html2;
                res.status(500).send(html);
        }
    })
})

router.post('/resend', function(req, res, next){
    authAPI.resend(req.body.email, function(reason){
        switch(reason){
            case 1:
                res.status(404).json("Link not valid");
                break;
            case 2:
                res.status(400).json("Link expired");
                break;
            case 3:
                res.status(200).json("Link resent");
                break;
            case 4:
                res.status(400).json("User already activated");
                break
            case 5:
                res.status(404).json("Email not recognized");
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/reset', function(req, res, next){
    if (!req.body.resetcode){
        authAPI.resetSend(req.body.email, function(reason){
            switch(reason){
                case 1:
                    res.status(404).json("User not found");
                    break;
                case 2:
                    res.status(400).json("User not activated");
                    break;
                case 3:
                    res.status(200).json("Reset code sent");
                    break;
                default:
                    res.status(500).json("Unknown error");
            }
        })
    } else {
        authAPI.reset(req.body.mail, req.body.resetcode, req.body.password, function(reason){
            switch(reason){
                case 1:
                    res.status(401).json("Weak password");
                    break;
                case 2:
                    res.status(404).json("Wrong reset code");
                    break;
                case 3:
                    res.status(400).json("Code expired");
                    break;
                case 4:
                    res.status(200).json("Password changed");
                    break;
                case 5:
                    res.status(404).json("User not found");
                    break;
                default:
                    res.status(500).json("Unknown error");
            }
        })
    }
});

/*router.post('/reset/:id', function(req, res, next){
    authAPI.reset(req.params['id'], req.body.password, function(reason){
        switch(reason){
            case 1:
                res.status(400).json("Bad quality password");
                break;
            case 2:
                res.status(404).json("Wrong reset code");
                break;
            case 3:
                res.status(400).json("Link expired");
                break;
            case 4:
                res.status(200).json("Password changed");
                break;
            case 5:
                res.status(404).json("User not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
})*/

module.exports = router;