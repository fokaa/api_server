/*
 * Different auth APIs, depends on auth.jwt module
 */

var user = require('../user/user.schema');
var bcrypt = require('bcrypt');
var currentUser = {};
var utils = require('../utils/helpers');
var activations = require('./activate.schema');
var resets = require('./reset.schema');

var authAPI = {
    currentUser: currentUser,
    login: async function(login){
        currentUser = {};
        if (login.email && login.password){
            var uzivatel = await user.findOne({'email': login.email});
            if (uzivatel){
                var compare = await bcrypt.compare(login.password, uzivatel.password)
                if (compare){
                    var ret = {};
                    ret.email = uzivatel.email;
                    ret.username = uzivatel.username;
                    ret.role = uzivatel.role;
                    ret._id = uzivatel._id.toString();
                    ret.activated = uzivatel.activated;
                    ret.disabled = ret.disabled;
                    currentUser = ret;
                    currentUser.disabled = uzivatel.disabled;
                    return ret;
                }
                return {};
            }
            return {}
        }
    },
    register: function(login, callback){
        var data = {};
        try {
            data.email = login.email;
            data.username = login.username;
            data.password = login.password;
        } catch(err){
            callback(1)
            return;
        }
        user.findOne({"email": data.email}, function(err, found){
            if (err){
                callback(1);
            } else if (found){
                if (found.activated){
                    callback(2);
                } else {
                    activations.findOne({email: found.email}, function(err, act){
                        if (err){
                            callback(1);
                        } else if (act && act.email && (act.expiration > Date.now())){
                            callback(3);
                        } else {
                            found.remove();
                            activations.remove({"email": data.email}, function(err){});
                            resets.remove({"email": data.email}, function(err){});
                            var reg = new user(data);
                            reg.save(function (err) {
                                if (err){
                                    callback(1)
                                } else {
                                    var code = utils.generateRandomString();
                                    var activation = new activations({"link": encodeURI(code), "email":data.email});
                                    activation.save(function(err){
                                        if (err) {
                                            console.error("Error saving activation link " + err);
                                            callback(1);
                                        } else {
                                            utils.sendEmail(encodeURI(code), null, data.email, data.username);
                                            callback(4);
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
            } else {
                var reg = new user(data);
                reg.save(function (err) {
                    if (err){
                        callback(1)
                    } else {
                        var code = utils.generateRandomString();
                        var fcul = Date.now() + 86400000;
                        var activation = new activations({"link": encodeURI(code), "email":data.email, expiration: fcul});
                        activation.save(function(err){
                            if (err){
                                console.error(err);
                            }
                        });
                        utils.sendEmail(encodeURI(code), null, data.email,data.username);
                        callback(4);
                    }
                });
            }

        })
    },
    newPassword:function(id, callback){
        user.findOne({'email': id.email}, function(err, login) {
            if (err){
                callback(1);
                return;
            }
            if (login){
                bcrypt.compare(id.old, login.password, function(err, result){
                    if (result === true) {
                        login.password = id.new;
                        login.save().then(function(err){
                            if (err.errors != undefined){
                                callback(1);
                            } else {
                                callback(3);
                            }
                        })
                    } else {
                        callback(2)
                        return;
                    }
                })
            }
        })
    },
    activate: function(id, callback) {
        activations.findOne({"link": id}, function(err, activ){
            if (err){
                callback(1);
            } else if (!activ) {
                callback(1);
            } else if (activ.expiration < Date.now()) {
                activ.remove();
                callback(2);
            } else {
                activ.remove();
                user.findOne({"email": activ.email}, function (err, login) {
                    if (err){
                        callback(3);
                    } else {
                        login.activated = true;
                        login.save(function(err){
                            if (err) {
                                console.error("Error saving user in activation " + err);
                                callback(1);
                            } else {
                                callback(4);
                            };
                        })
                    }
                })
            }
        })
    },
    resend: function(email, callback){
        user.findOne({"email": email},function(err, login){
            if (err || !login){
                activations.findOne({"email":email}, function(err, res){
                    if (res && res.email) {
                        res.remove();
                    }
                    callback(5);
                })
            } else {
                if (login.activated){
                    activations.findOne({"email":email}, function(err, res){
                        if (res && res.email) {
                            res.remove();
                        }
                        callback(4)
                    })
                } else {
                    activations.findOne({"email":email}, function(err, res){
                        if (err || !res){
                            callback(1);
                        } else if (res.expiration < Date.now()) {
                            res.remove();
                            callback(2);
                        } else {
                            utils.sendEmail(encodeURI(res.link), null, email, email);
                            callback(3);
                        }
                    });
                }
            }
        })
    },
    resetSend: function(email, callback){
        if (email == "admin"){
            callback(100);
        } else {
            user.findOne({"email": email},function(err, login){
                if (err || !login){
                    resets.findOne({"email":email}, function(err, res){
                        if (res && res.email) {
                            res.remove();
                        }
                        callback(1);
                    })
                } else if (!login.activated){
                    callback(2);
                } else {
                    resets.findOne({"email":login.email}, function(err, reset){
                        if (err){
                            callback(6)
                        } else {
                            if (reset) {
                                reset.remove();
                            }
                            var code = utils.generateRandomString(10);
			                var exp = new Date();
			                exp = exp.valueOf() + 3600000
                            var reset = new resets({"link": encodeURI(code), "email": email, "expiration":exp});
                            reset.save(function(err){
                                if (err) {
                                    console.error("Error saving in activation resend " + err);
                                    callback(6);
                                } else {
                                    utils.sendEmail(encodeURI(code), true, email, email);
                                    callback(3);
                                }
                            });
                        };
                    });
                }
            });
        }
    },
    reset: function(mail, id, password, callback){
        if (!password || !utils.checkPasswordQuality(password)){
            callback(1);
        } else {
            resets.findOne({"link": id}, function(err, link){
                if (err || !link || !link.email){
                    callback(2);
                } else if (link.expiration < Date.now()) {
                    link.remove();
                    callback(3);
                } else {
                    link.remove();
                    user.findOne({"email": link.email}, function(err, login){
                        if (err || !login){
                            callback(5);
                        } else {
                            login.password = password;
                            login.save(
                                function(err){
                                    if (err) {
                                        console.error("Error saving in reset " + err);
                                        callback(3);
                                    } else {
                                        callback(4);
                                    }

                                }
                            );

                        }
                    })
                }
            })
        }
    }

}

module.exports = authAPI;
