/*
 * management of JWT codes
 */


const config = require('../../config.json');

if (config.security && config.security.secret && (config.security.secret != "")){
    var SECRET = config.security.secret;
} else {
    var SECRET = "SECRETO_PARA_ENCRIPTACION";
}

const authAPI = require('./auth.api');
const userAPI = require('../user/user.api');

const jwt = require('jsonwebtoken');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

var refreshTokens = {};

var login = async function(requser){
    if (requser.email && requser.password){
        var user = await authAPI.login(requser);
        if (user._id && user.activated && !user.disabled){
            if (!requser.robot) {
                var token = jwt.sign({
                    '_id':user._id,
                    "username":user.username,
                    "email":user.email,
                    "role":user.role
                }, SECRET, { expiresIn: config.security.jwt_exp });
            } else {
                var token = jwt.sign({
                    '_id':user._id,
                    "username":user.username,
                    "email":user.email,
                    "role":user.role,
                    "rAddress":requser.robotAddress
                }, SECRET, { expiresIn: config.security.jwt_exp });
            }
            
            if (!requser.robot) {
                var refreshToken = jwt.sign({
                    '_id':user._id
                }, SECRET, { expiresIn: config.security.jwt_refresh_exp });
            } else {
                var refreshToken = jwt.sign({
                    '_id':user._id,
                    "rAddress":requser.robotAddress
                }, SECRET, { expiresIn: config.security.jwt_refresh_exp });
            }
            
            if (refreshToken in refreshTokens){
                delete refreshTokens[refreshToken];
            }

            refreshTokens[refreshToken] = user._id;

            return {
                "token":token,
                "refreshToken": refreshToken
            }

        } else {
            return {};
        }
    } else {
        return {};
    }
}

var logout = function(id){
    for (token in refreshTokens) {
        if (refreshTokens[token] == id){
            delete refreshTokens[token];
        }
    }
}

var refresh = function (token, callback) {
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err || !decoded || !decoded._id){
            callback(1);
        } else if ((decoded.exp*1000) < Date.now()){
            callback(1);
        }
        else {
            userAPI.getUser(decoded._id, function(reason, user){
                if (reason == 2){
                    var token = jwt.sign({
                        '_id':user._id,
                        'username': user.username,
                        'role': user.role,
                        "email":user.email,
                        "role":user.role
                    }, SECRET, { expiresIn: config.security.jwt_exp });
                    callback(2, token);
                } else {
                    callback(1);
                }
            })
        }
    });
};

var verifyJwt = function (token) {
    var decoded;
    try {
	decoded = jwt.verify(token, SECRET);
    } catch (err) {
        return false; 
    }
    if ((decoded.exp*1000) < Date.now()){
        return false; 
    }
    return true;
};

var reject = function(token){
    if (refreshTokens[token]){
        delete refreshTokens[token];
        return true;
    }
    return null;
};

var rolesAuth = function(roles){
    return function(req, res, next){
        var user = req.user;
        if (roles.length == 0){
            return next();
        } else if (roles.indexOf(user.role) > -1){
            return next();
        } else {
            res.status(401).json("Not authorized to access this content");
            return next('Unauthorized');
        }
    }
};

passport.serializeUser(function (user, done) {
    done(null, user._id);
});


passport.deserializeUser(function (_id, done) {
    done(null, _id);
});

var opts = {};

// Setup JWT options
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = SECRET;

passport.use(new JwtStrategy(opts, function (jwtPayload, done) {
    userAPI.isDisabled(jwtPayload._id).then(function(disabled){
        if (disabled.disabled) {
            return done(null, false);
        }
        if (!disabled.activated){
            return done(null, false);
        }
        var expirationDate = new Date(jwtPayload.exp * 1000);
        if(expirationDate < new Date()) {
            return done(null, false);
        }
        var user = jwtPayload;
        done(null, user);
    })
}))

var exp = {
    "login": login,
    "logout": logout,
    "refresh": refresh,
    "reject": reject,
    "rolesAuth": rolesAuth,
    "passport": passport,
    "verifyJwt": verifyJwt
};

module.exports = exp;
