var mongoose = require('mongoose');

var ResetSchema = new mongoose.Schema({
    link: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    expiration: {
        type: Date
    }
});

ResetSchema.set("timestamps", true);

var reset = mongoose.model('Reset', ResetSchema);

module.exports = mongoose.model('Reset');