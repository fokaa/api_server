/* Different expensive APIs to be redesigned in the future
 *
*/

const router = require('express').Router();
const bodyParser = require('body-parser');

const jwt = require('../auth/auth.jwt');

const andrejAPI = require('./andrej.api');
const userAPI = require('../user/user.api');
const deviceAPI = require('../device/device.api');
const midd = require('../../security/middleware')

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(jwt.passport.initialize());
router.use(jwt.passport.session());

router.get('/profile', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    userAPI.getUser(req.user._id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(200).json(data);
                break;
            case 3:
                res.status(404).json("User not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/profile/favorites', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    userAPI.getUserFavorites(req.user._id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(200).json(data);
                break;
            case 3:
                res.status(404).json("User not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/profile/favorites/:id', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    userAPI.addUserFavorites(req.user._id, req.params.id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(200).json({"favorites":data});
                break;
            case 3:
                res.status(404).json("Not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.delete('/profile/favorites/:id', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    userAPI.deleteUserFavorites(req.user._id, req.params.id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(200).json({"favorites":data});
                break;
            case 3:
                res.status(404).json("Not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/devices', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    deviceAPI.getAllDevices(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/devices/owned', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    deviceAPI.getDevices(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/devices/shared', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    deviceAPI.getSharedDevices(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/overview', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    if ((req.query) && (req.query.sensors)){
        var query = req.query.sensors.split(" ");
    }
    andrejAPI.getOverview(req.user, "all", query, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/overview/owned', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    if ((req.query) && (req.query.sensors)){
        var query = req.query.sensors.split(" ");
    }
    andrejAPI.getOverview(req.user, "owned" , query, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/overview/shared', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    if ((req.query) && (req.query.sensors)){
        var query = req.query.sensors.split(" ");
    }
    andrejAPI.getOverview(req.user, "shared" , query, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/overview/favorites', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    if ((req.query) && (req.query.sensors)){
        var query = req.query.sensors.split(" ");
    }
    andrejAPI.getOverview(req.user, "favorites" , query, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/overview/:id', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    if ((req.query) && (req.query.sensors)){
        var query = req.query.sensors.split(" ");
    }
    andrejAPI.getOverview(req.user, req.params.id , query, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 2:
                res.status(401).json("No authorized to read the device");
                break;
            case 4:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

module.exports = router;
