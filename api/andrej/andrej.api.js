const tsAPI = require('../telemetry/ts.api');
const devices = require("../device/device.api");
//Telemetry overview
function getTSOverview(devices, meas, callback){
    tsAPI.getLastOverview(devices, meas, function(reason, data){
        if (reason == 4){
            //process data
            callback(data);
        } else {
            callback(devices);
        }
    })
}

const andrejAPI = {
    getOverview: function(user, type, meas, callback){
        switch (type){
            case "all":
                devices.getAllDevices(user, function(reason, data){
                    if (reason == 3){
                        getTSOverview(data, meas, function(devices){
                            callback(3, devices);
                        })
                    } else {
                        callback(reason);
                    }
                })
                break;
            case "shared":
                devices.getSharedDevices(user, function(reason, data){
                    if (reason == 3){
                        getTSOverview(data, meas, function(devices){
                            callback(3, devices);
                        })
                    } else {
                        callback(reason);
                    }
                })
                break;
            case "owned":
                devices.getDevices(user, function(reason, data){
                    if (reason == 3){
                        getTSOverview(data, meas, function(devices){
                            callback(3, devices);
                        })
                    } else {
                        callback(reason);
                    }
                })
                break;
            case "favorites":
                devices.getFavoriteDevices(user, function(reason, data){
                    if (reason == 3){
                        getTSOverview(data, meas, function(devices){
                            callback(3, devices);
                        })
                    } else {
                        callback(reason);
                    }
                })
                break;
            default:
                devices.getDevice(user, type, function(reason, data){
                    if (reason == 4){
			//console.log(user, type)
                        getTSOverview(data, meas, function(devices){
                            callback(4, devices);
                        })
                    } else {
                        callback(reason);
                    }
                })
        }
    }
}

module.exports = andrejAPI;
