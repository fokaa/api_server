/*
 * API to access device data with external tools
*/

const router = require('express').Router()
const bodyParser = require('body-parser')

const deviceApi = require('../device/token/device.token.api')
const tsAPI = require('../telemetry/ts.api')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))


var apiAuth = function(){
    return async function(req, res, next){
        var code = req.get('authorization').split(' ').pop();
        try {
            let token = await deviceApi.checkDeviceTokenAsync(req.params.id, code);    
            if (token) {
                req.device = token;
                return next();
            }
        } catch (error) {
            res.status(401).json("Not authorized");
            return next('Unauthorized');
        }
    }
};

router.get('/:id', apiAuth(), function(req, res, next){
    let from, to;
    if (req.query.from && req.query.to){
        from = parseInt(req.query.from, 10)
        to = parseInt(req.query.to, 10)
        if (isNaN(from) || isNaN(to) || from > to) {
            res.status(400).json("Bad query");
            return;
        }
    } else {
        res.status(400).json("Bad query");
        return;
    }
    var rt = "time", rc = "d", w = "isoweek"
    if (req.query){
        if (req.query.meas){
            var query = req.query.meas.split(" ");
        }
        if (req.query.sensors){
            var query = req.query.sensors.split(" ");
        }
        if (req.query.rt){
            rt = req.query.rt;
        }
        if (req.query.rc){
            rc = req.query.rc;
        }
        if (req.query.w){
            w = req.query.w;
        }
    }
    tsAPI.getExternalApiData( req.device, from, to, query, rt, rc, w, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized to access device");
                break;
            case 4:
                res.status(200).json(data);
                break;
            case 5:
                res.status(400).json("Bad query");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    });
});

module.exports = router;
