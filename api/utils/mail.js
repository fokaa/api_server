const nodemailer = require('nodemailer');
const fs = require('fs');
const mustache = require('mustache');

let gmail = {
    service: 'gmail',
    auth: {
           user: '',
           pass: ''
       }
};

let transporter = nodemailer.createTransport(gmail);

transporter.verify(function(error, success) {
    if (error) {
        console.error(new Date().toLocaleString(), error);
        process.exit(1);
    } else {
        console.log(new Date().toLocaleString(),'Mail server connected successfully');
    }
});

var mail = {
    sendMail: function(to, subject, musObj){
        if (musObj.register){
            fs.readFile(__dirname + "/emails/register.html", function (err, data) {
                if (err) {
                    console.error(new Date().toLocaleString(),"Cannot read email templates!");
                    var output = "To activate your account, please click on following link: \n " + musObj.link;
                } else {
                    var html = data.toString()
                    var output = mustache.render(html, musObj);
                }
                var message = {
                    from: 'peter.stolc@corryn.net',
                    to: to,
                    subject: subject,
                    text: "To activate your account, please click on following link: \n " + musObj.link,
                    html: output
                };
                transporter.sendMail(message, function(err, info){
                    if (err){
                        console.error(new Date().toLocaleString(), info);
                    }
                });
              });
        } else {
	        fs.readFile(__dirname + "/emails/reset.html", function (err, data) {
                if (err) {
                    console.error(new Date().toLocaleString(),"Cannot read email templates!");
                    var output = "To reset your password, please use following code: \n " + musObj.link;
                } else {
                    var html = data.toString()
                    var output = mustache.render(html, musObj);
                }
                var message = {
                    from: 'peter.stolc@corryn.net',
                    to: to,
                    subject: subject,
                    text: "To reset your password, please use following code: \n " + musObj.link,
                    html: output
                };
                transporter.sendMail(message, function(err, info){
                    if (err){
                        console.error(new Date().toLocaleString(), info);
                    }
                });
            });
        }
    }
};

module.exports = mail;
