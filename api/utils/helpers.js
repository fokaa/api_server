const mail = require('./mail');
const config = require('../../config');

function getRandomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
}

function generateRandomString(length) {
    var tokens = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        chars = 5,
        segments = 4,
        delimiter = '',
        keyString = Date.now() + delimiter;

    if (length){
        chars = length;
        segments = 1;
    }

    for( var i = 0; i < segments; i++ ) {
        var segment = "";

        for( var j = 0; j < chars; j++ ) {
            var k = getRandomInt( 0, 35 );
            segment += tokens[ k ];
        }

        keyString += segment;

        if( i < ( segments - 1 ) ) {
            keyString += delimiter;
        }
    }

    return keyString;
}

function checkPasswordQuality(pass){
    //TODO: Clarify with Jano 
    if (!pass || pass.length < 8){
        return false
    } else {
        return true;
    }
};

function checkEmail(emailAddress) {
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(emailAddress);
};

function sendEmail(link, code, email, username){
    if (code){
        //to, subject, text
        //link = "Your reset code is " + link;
        var musObj = {
            "register":false,
            "email": email,
            "link": link
        }
        mail.sendMail(email, "allMETEO Weather Portal: Reset your password", musObj);
        //console.log("Sending email with reset code " + link);
    } else {
        var musObj = {
            "register":true,
            "email": email,
            "username": username,
            "link":config.server.extaddress + "/auth/activate/" + link
        }
        //link = "To activate your account, please click on following link: \n " + config.server.extaddress + "/auth/activate/" + link;
        mail.sendMail(email, "allMETEO Weather Portal: Please activate your account", musObj);
        //console.log("Sending email with link " + link);
    }

}

function deepUpdate(obj1, obj2) {
    if (typeof obj1 == "undefined"){
        obj1 = {};
    }
    for (var prop in obj2) {
        try {
            if ( obj2[prop].constructor === Object) {
                obj1[prop] = deepUpdate(obj1[prop], obj2[prop])
            } else {
                obj1[prop] = obj2[prop];
            }
        } catch(e) {
            obj1[prop] = obj2[prop];
        }
    }
    return obj1;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = {
    "generateRandomString":generateRandomString,
    "checkEmail":checkEmail,
    "checkPasswordQuality": checkPasswordQuality,
    "sendEmail":sendEmail,
    "deepUpdate": deepUpdate,
    "sleep": sleep
}
