/*
 * Socket.io specific code
 */


const config = require('../../config');
const redis = require('../../db/redis');
const nonSubRedis = require('../../db/nonSubRedis');
const verifyJwt = require('../auth/auth.jwt').verifyJwt; 
const redisSocket = require('socket.io-redis')
const socketio = require('socket.io');

const socket = {
    io: null,
    clients: 0,
    init(http) {
        var self = this;
        try {
            self.io = socketio(http, {
                path: '/ws'
            })
	    self.io.set('transports', ['websocket', 'polling']);
            self.io.adapter(redisSocket({ host: config.redis.host, port: config.redis.port }))
        } catch (error) {
            console.error(new Date().toLocaleString(), "socket.io error " + error); 
        }

        self.io.on('connection', (socket) => {
            self.clients++;

            socket.on('station', (room) => {
                let rooms = Object.keys(socket.rooms);
                if (rooms.length > 1) {
                    for (let i = 1; i < rooms.length; i++) {
                        socket.leave(rooms[i]);
                    }
                }
                socket.join(room);
                data = nonSubRedis.get(room, (error, data) => {
                    if (!error && data) {
			data = JSON.parse(data);
			data.pm_id = process.env.pm_id;
			data = JSON.stringify(data);
                        socket.emit('data', data);
                    } else if (!data) {
                        socket.disconnect();
                    }
                })
            })

	    socket.on('favorites', (data) => {
                data = JSON.parse(data);
		if (!verifyJwt(data.token)){
		   let rooms = Object.keys(socket.rooms);
		   if (rooms.length > 1) {
                        var index = rooms.indexOf(socket.id);
                        if (index !== -1){
                            rooms.splice(index,1);
                        }
                        for (let i = 1; i < rooms.length; i++) {
                            socket.leave(rooms[i]);
                        }
                    }
		} else {
	            var fList = data.favorites;	
                
                    if (Array.isArray(fList)) {
                    let rooms = Object.keys(socket.rooms);
                    if (rooms.length > 1) {
			var index = rooms.indexOf(socket.id);
			if (index !== -1){
			    rooms.splice(index,1);
			}
                        for (let i = 1; i < rooms.length; i++) {
			    index = fList.indexOf(rooms[i]);
			    if (index == -1){
			    	socket.leave(rooms[i]);
			    } else {
				fList.splice(index, 1);
			    }
                        }
			if (fList.length > 0) {
			    socket.join(fList, (err)=>{
                        	if (err){
                            	    //console.error("error joining ", err)
                        	} else {
                        	}
                    	    });
			}
                    } else {
			socket.join(fList, (err)=>{
                            if (err){
                                //console.error("error joining ", err)
                            } else {
                            }
                        });
		    }
                    }
		}
            })

            socket.on('disconnect', (reason) => {
                self.clients--;
            });

        })
        if (self.io && process.env.pm_id == 1) {
            redis.subscribe(config.redis.channel, function (err, count) { 
                if (!err) {
                    console.log(new Date().toLocaleString(),'Connected to redis channel ', config.redis.channel);
                } else {
		    console.error(new Date().toLocaleString(),'Err connecting to ', config.redis.channel, " ", err);
		}
            })
	    //console.log(self.io)
            
            redis.on('message', function (channel, message) {
		var msg = JSON.parse(message);
                var room = msg.sn;
		msg.pm_id = process.env.pm_id
		message = JSON.stringify(msg)
                try {
                    self.io.in(room).clients((err, listc) => {
                         if (!err && listc.length > 0) {
                            self.io.in(room).emit('data', message);
                        }
                    })
                } catch (error) {
                }
            });
        }
    }
}

module.exports = socket;

