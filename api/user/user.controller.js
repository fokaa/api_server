/*
 * User management
 */

const router = require('express').Router();
const bodyParser = require('body-parser');

const jwt = require('../auth/auth.jwt');
const userAPI = require('./user.api');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(jwt.passport.initialize());
router.use(jwt.passport.session());

router.get('/', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin', 'user']) ,function(req, res, next){
    userAPI.getUsers(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(404).json("No data");
                break;
            case 2:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/additional',  jwt.passport.authenticate('jwt'), function(req, res, next){
    userAPI.getUserAdditionalInfo(req.user._id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("No data returned");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.put('/additional',  jwt.passport.authenticate('jwt'), function(req, res, next){
    userAPI.saveUserAdditionalInfo(req.user._id, req.body, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("No data returned");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/additional',  jwt.passport.authenticate('jwt'), function(req, res, next){
    userAPI.saveUserAdditionalInfo(req.user._id, req.body, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("No data returned");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/:id', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin', 'user']) ,function(req, res, next){
    userAPI.getUser(req.params.id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(200).json(data);
                break;
            case 3:
                res.status(404).json("User not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.delete('/:id', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']) ,function(req, res, next){
    id = req.params.id;
    userAPI.deleteUser(id, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("User not found");
                break;
            case 3:
                res.status(200).json("User deleted");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/',  jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']) ,function(req, res, next){
    userAPI.createUser(req.body, function(reason){
        switch(reason){
            case 1:
                res.status(400).json("Bad data");
                break;
            case 2:
                res.status(200).json("User created");
                break;
            case 3:
                res.status(401).json("User already exists");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.put('/',  jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']) ,function(req, res, next){
    userAPI.modifyUser(req.body, function(reason){
        switch(reason){
            case 1:
                res.status(404).json("User not found");
                break;
            case 2:
                res.status(200).json("User modified");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});



router.get('/:id/disable', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']) ,function(req, res, next){
    userAPI.disable(req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(200).json("User disabled");
                break;
            case 2:
                res.status(404).json("User not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/:id/enable', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']) ,function(req, res, next){
    userAPI.enable(req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(200).json("User enabled");
                break;
            case 2:
                res.status(404).json("User not found");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

module.exports = router;
