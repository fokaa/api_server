/*
 * User management
 */

const user = require('./user.schema');
const devices = require('../device/device.schema');
const bcrypt = require('bcrypt');
const helpers = require('../utils/helpers')

const userAPI = {
     getUser: function (id, callback) {
        if (id){
            user.findById(id, {"password":0, "disabled":0, "activated":0, "__v":0 }, function(err, login) {
                if (err){
                    callback(1);
                } else if (login){
                    if (login.favorites){
                        login.favorites.forEach((fav) => {delete fav._doc._id});
                    }
                    callback(2, login);
                } else{
                    callback(3);
                }
            })
        } else {
            callback(1);
        }
    },
    getUserFavorites: function (id, callback) {
        if (id){
            user.findById(id, {"favorites": 1, "_id":0}, {lean: true},  function(err, login) {
                if (err){
                    callback(1);
                } else if (login){
                    let ret = login.favorites;
                    let fav;
                    ret.forEach((fav) =>
                    {delete fav._id});
                    callback(2, ret);
                } else{
                    callback(3);
                }
            })
        } else {
            callback(1);
        }
    },
    addUserFavorites: function (id, favorite, callback) {
        if (id){
            user.findById(id, function(err, login) {
                if (err){
                    callback(1);
                } else if (login && login.favorites){
                    for (let x = 0; x < login.favorites.length; x++){
                        if (favorite == login.favorites[x].sn){
                            callback(2, login.favorites);
                            return;
                        }
                    }
                    devices.findOne({"sn":favorite}, function(err, device){
                        if (err){
                            callback(1);
                        } else if (device && device.name && device.owner){
                            user.findById(device.owner, function(err, owner){
                                if (err){
                                    callback(1);
                                } else if (owner && owner.username) {
                                    login.favorites.push({
                                        sn: device.sn,
                                        devicename: device.name,
                                        username: owner.username
                                    });
                                    login.save(function(err){
                                        if (err){
                                            callback(1);
                                        } else {
                                            var ret = login.favorites;
                                            let fav;
                                            ret.forEach((fav) => {delete fav._doc._id});
                                            callback(2, ret);
                                        }
                                    })
                                } else {
                                    callback(3);
                                }
                            })
                        } else {
                            callback(3);
                        }
                    })
                    //callback(2, login);
                } else{
                    callback(3);
                }
            })
        } else {
            callback(1);
        }
    },
    deleteUserFavorites: function (id, favorite, callback) {
        if (id){
            user.findById(id, function(err, login) {
                if (err){
                    callback(1);
                } else if (login && login.favorites){
                    var found = false;
                    for (let x = 0; x < login.favorites.length; x++){
                        if (favorite == login.favorites[x].sn){
                            found = true;
                            login.favorites.splice(x, 1);
                            login.save(function(err){
                                if (err){
                                    callback(1);
                                    return;
                                } else {
                                    var ret = login.favorites;
                                    let fav;
                                    ret.forEach((fav) => {delete fav._doc._id});
                                    callback(2, ret);
                                    return;
                                }
                            })
                            break;
                        }
                    }
                    if (!found){
                        callback(2, login.favorites);
                    }
                } else{
                    callback(3);
                }
            })
        } else {
            callback(1);
        }
    },
    getUserAdditionalInfo: function(id, callback){
        if (id){
            user.findById(id, function(err, data){
                if (err){
                    callback(1);
                } else if (!data){
                    callback(2);
                } else if (!data.additionalInfo){
                    callback(3, {});
                } else {
                    callback(3, data.additionalInfo);
                }
            })
        }
    },
    saveUserAdditionalInfo: function(id, info, callback){
        if (id && info.additionalInfo){
            user.findById(id, function(err, data){
                if(err){
                    callback(1);
                } if (!data) {
                    callback(2);
                } else {
                    data.markModified('additionalInfo')
                    data.additionalInfo = helpers.deepUpdate(data.additionalInfo, info.additionalInfo);
                    data.save(function(err){
                        if (err){
                            callback(1)
                        } else {
                            callback(3, data.additionalInfo)
                        }
                    });
                }
            });
        }
    },
    getUsers: function(uziv, callback) {
         if (uziv.role == 'admin'){
             user.find({}, { "password":0, "__v":0 }, function(err, data){
                 if (err) {
                     callback(3)
                 } else if (!data){
                     callback(1)
                 } else {
                     data.forEach((us)=>delete us.password);
                     callback(2, data);
                 }
             })
         } else {
             user.findOne({email: uziv.email}, {"password":0, "disabled":0, "activated":0, "__v":0 }, function(err, data){
                 if (err) {
                     callback(3)
                 } else if (!data){
                     callback(1)
                 } else {
                     delete data.password;
                     delete data.disabled;
                     delete data.activated;
                     if (data.favorites){
                         data.favorites.forEach((fav) => {delete fav._doc._id});
                     }
                     callback(2, data);
                 }
             })
         }
    },
    deleteUser: function(id, callback){
        user.findById(id, function(err, data){
            if (err){
                callback(1);
            } else if (!data){
                callback(2)
            }
            else {
                data.remove();
                callback(3);
            }
        })
    },
    createUser: function(data, callback){
         if (data && data.username && data.password && data.email){
             var uziv = new user(data);
             uziv.save(function(err){
                 if (err){
                     if (err.code == 11000){
                         callback(3);
                     } else {
                         callback(4);
                     }
                 } else {
                     callback(2);
                 }
             })
         } else {
             callback (1);
         }
    },
    modifyUser: function(data, callback){
        if (data.id){
            user.findById(data.id, function(err, uziv){
                if (err){
                    callback(3);
                } else if (uziv){
                    if (data.username){
                        uziv.username = data.username;
                    }
                    if (data.role) {
                        uziv.role = data.role;
                    }
                    uziv.save(function(err){
                        if (err){
                            callback(3)
                        } else {
                            callback(2)
                        }
                    });
                } else {
                    callback(1)
                }
            })
        } else if (data.email){
            user.findOne({email: data.email}, function(err, uziv){
                if (err){
                    callback(3);
                } else if (uziv){
                    if (data.username){
                        uziv.username = data.username;
                    }
                    if (data.role) {
                        uziv.role = data.role;
                    }
                    uziv.save(function(err){
                        if (err){
                            callback(3)
                        } else {
                            callback(2)
                        }
                    });
                } else {
                    callback(1)
                }
            })
        }
    },
    disable: function(id, callback){
        user.findById(id, function(err, uziv){
            if (err){
                callback(3);
            } else if (!uziv){
                callback(2);
            } else {
                uziv.disabled = true;
                uziv.save(function (err) {
                    if (err) {
                        console.error("Error saving user in disable " + err);
                        callback(3);
                    } else {
                        callback(1);
                    }
                });
                callback(1);
            }
        })
    },
    enable: function(id, callback){
        user.findById(id, function(err, uziv){
            if (err){
                callback(3);
            } else if (!uziv){
                callback(2);
            } else {
                uziv.disabled = false;
                uziv.save(function (err) {
                    if (err) {
                        console.error("Error saving user in enable " + err);
                        callback(3);
                    } else {
                        callback(1);
                    }
                })
            }
        })
    },
    isDisabled: function(id){
        if (id){
            return user.findById(id);
        }
     }
}

module.exports = userAPI;
