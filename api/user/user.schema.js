var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var config = require('../../config.json');

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: "user"
    },
    disabled: {
        type: Boolean,
        default: false
    },
    activated: {
        type: Boolean,
        default: false
    },
    picture:{
        type: Buffer,
        contentType: String
    },
    subscription:[{type: String}],
    additionalInfo: {
        type: mongoose.Schema.Types.Mixed
    },
    favorites:[
        {
            sn: {
                type: String,
                required: true,
                trim: true
            },
            username:{
                type: String,
                required: true,
                trim: true
            },
            devicename:{
                type: String,
                required: true,
                trim: true
            }
        }
    ]
});

UserSchema.pre('save', function (next) {
    var user = this;
    if (user["$__"].activePaths.paths.password == "init") {
        next();
    } else {
        bcrypt.hash(user.password, 10, function (err, hash){
            if (err) {
                return next(err);
            }
            user.password = hash;
            next();
        })
    }
});

UserSchema.set("timestamps", true);

var user = mongoose.model('User', UserSchema);

user.countDocuments({ email: config.admin.email }, function (err, count) {
    if (count === 0){
        var root = new user({email: config.admin.email, username: config.admin.username, password: config.admin.password, role: "admin", "activated": true});
        root.save(function (err) {
            if (err){
                process.exit(1);
            }
        });
    }
});

module.exports = mongoose.model('User');
