const GeoJSON = require('mongoose-geojson-schema');
const mongoose = require('mongoose');

var DevSchema = new mongoose.Schema({
    sn:{
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    name:{
        type: String
    },
    position: {
        type: mongoose.Schema.Types.Point
    },
    elevation: {
	    type: Number,
	    default: 0
    },
    height: {
        type: Number,
	    default: 0
    },
    type:{
        type: String,
        trim: true
    },
    provider:{
        type: String,
        trim: true
    },
    deviceId:{
        type: String,
        trim: true
    },
    picture:{
        type: Buffer,
        contentType: String
    },
    shared: {
        type: Boolean,
        default: true
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    disabled: {
        type: Boolean,
        default: false
    },
    tags:[
        {
            type: String,
            trim: true
        }
    ],
    comments: [
        {
            author:{
                type: String,
                required: true
            },
            text: {
                type:String,
                required: true
            }
        }
    ],
    weather_icon:{
        type: String,
        trim: true,
        default: ""
    },
    forecast:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Weather'
    },
    settings:{
        type: mongoose.Schema.Types.Mixed,
        default: {}
    }
});

DevSchema.index({ "sn": 1 }, { unique: true });

DevSchema.set("timestamps", true);

DevSchema.pre('validate', function (next) {
    var device = this;
    if (device.errors && device.errors.position && Array.isArray(device.errors.position.value) && (device.errors.position.value.length == 2)){
        var pos = device.errors.position.value;
        device.position = {
            type: "Point",
            coordinates: [pos[1], pos[0]]
        }
        delete device.errors;
        delete device.$__.validationError;
    }
    next();
});

DevSchema.pre('save', function (next) {
    var device = this;
    if (!device.name || device.name == ""){
        device.name = device.sn;
    }
    next();
});

/*DevSchema.post('findOne', async function(docs) {
    console.log(this.model);

    if (this.Schema.pop)
        console.log(this)
});
*/

var device = mongoose.model('Device', DevSchema);

module.exports = mongoose.model('Device');
