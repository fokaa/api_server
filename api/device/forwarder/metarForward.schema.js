const mongoose = require('mongoose');

const MetarForward = new mongoose.Schema({
    //server info login:passowrd@server:port/path
    server:{
        type: String,
        required: true,
        trim: true
    },
    port:{
        type: String,
        required: true,
        trim: true
    },
    path:{
        type: String,
        required: true,
        default: '/',
        trim: true
    },
    login:{
        type: String,
        trim: true
    },
    password:{
        type: String,
        trim: true
    },
    //last result and time
    last:{
        type: String
    },
    lasttime:{
        type: Date
    },
});

MetarForward.set("timestamps", true);

const forwarder = mongoose.model('metarForward', MetarForward);

module.exports = mongoose.model('metarForward');