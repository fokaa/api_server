/*
 * manage device forwarders
 */

const fwds = require('./forward.schema')
const devices = require('../device.schema');

const fwdAPI = {
    getForwarders: function(user, devId, callback){
        try {
            if (user.role != "admin"){
                fwds.find({ "user": user.email, "sn": devId },{"log": 0, "_id": 0 }, { lean: true }, (err, data) => {
                    if (err) {
                        callback(err, null)
                    } else {
                        callback(null, data)
                    }
                })
            } else {
                fwds.find({ "sn": devId },{"log": 0, "_id": 0 }, { lean: true }, (err, data) => {
                    if (err) {
                        callback(err, null)
                    } else {
                        callback(null, data)
                    }
                })
            }
            
        } catch (error) {
            callback(error, null)
        }
    },
    getForwarder: function(user, devId, name, callback){
        try {
            if (user.role != "admin"){
                fwds.findOne({"user": user.email, "sn": devId, "name": name}, {"log": 0, "_id": 0 }, { lean: true }, (err, data) => {
                    if (err) {
                        callback(err)
                    } else if (!data){
                        callback("Forwarder not found")
                    } else {
                        callback(null, data)
                    }
                })
            } else {
                fwds.findOne({"sn": devId, "name": name}, {"log": 0, "_id": 0 }, { lean: true }, (err, data) => {
                    if (err) {
                        callback(err)
                    } else if (!data){
                        callback("Forwarder not found")
                    } else {
                        callback(null, data)
                    }
                })
            }
        } catch (error) {
            callback(error, null)
        }
    },
    getForwarderLogs: function(user, devId, name, callback){
        try {
            if (user.role != "admin"){
                fwds.findOne({"user": user.email, "sn": devId, "name": name}, {"log": 1}, { lean: true }, (err, data) => {
                    if (err) {
                        callback(err)
                    } else if (!data){
                        callback("Forwarder not found")
                    } else {
                        callback(null, data)
                    }
                })
            } else {
                fwds.findOne({"sn": devId, "name": name}, {"log": 1}, { lean: true }, (err, data) => {
                    if (err) {
                        callback(err)
                    } else if (!data){
                        callback("Forwarder not found")
                    } else {
                        callback(null, data)
                    }
                })
            }
        } catch (error) {
            callback(error, null)
        }
    },
    postForwarders: function(user, devId, forwarder, callback){
        if (forwarder.sn != devId){
            forwarder.sn = devId
        }
        forwarder.user = user.email
        try {
            devices.findOne({"sn":devId}, {}, { lean: true }, (err, device) => {
                if (err) {
                    callback(err)
                } else if (!device) {
                    callback("Device not found")
                } else if (device.owner != user._id && user.role != 'admin') {
                    callback("Not permitted")
                } else {
                    var forw = new fwds(forwarder)
                    forw.save((err) => {
                        if (err){
                            if (err.message){
                                callback(err.message)
                            } else {
                                callback(err)
                            }
                        } else {
                            callback(null, forw)
                        }
                    })
                }
            })
        } catch (error) {
            callback(error)
        }
    },
    putForwarders: function(user, devId, name, forwarder, callback){
        try {
            if (user.role != "admin"){
                fwds.findOne({"user": user.email, "sn": devId, "name": name}, (err, data) => {
                    if (err) {
                        callback(err)
                    } else if (!data){
                        callback("Forwarder not found")
                    } else {
                        if (typeof forwarder.enabled !== 'undefined'){
                            data.enabled = forwarder.enabled
                        }
                        if (forwarder.type){
                            data.type = forwarder.type
                        }
                        if (forwarder.server){
                            data.server = forwarder.server
                        }
                        if (forwarder.port){
                            data.port = forwarder.port
                        }
                        if (forwarder.login){
                            data.login = forwarder.login
                        }
                        if (forwarder.password){
                            data.password = forwarder.password
                        }
                        if (forwarder.path){
                            data.path = forwarder.path
                        }
                        if (forwarder.mappings){
                            data.mappings = forwarder.mappings
                        }
                        if (forwarder.newName){
                            data.name = forwarder.newName
                        }
                        if (forwarder.offset){
                            data.offset = forwarder.offset
                        }
                        if (forwarder.header){
                            data.header = forwarder.header
                        }
                        if (forwarder.format){
                            data.format = forwarder.format
                        }
                        if (forwarder.filename) {
                            data.filename = forwarder.filename
                        }
                        if (forwarder.headers && Array.isArray(forwarder.headers)) {
                            data.headers = forwarder.headers
                        }
                        if (forwarder.airport){
                            data.airport = forwarder.airport
                        }
                        data.save((err) => {
                            if (err){
                                if (err.message){
                                    callback(err.message)
                                } else {
                                    callback(err)
                                }
                            } else {
                                callback(null, data)
                            }
                        })
                    }
                })
            } else {
                fwds.findOne({"sn": devId, "name": name}, (err, data) => {
                    if (err) {
                        callback(err)
                    } else if (!data){
                        callback("Forwarder not found")
                    } else {
                        if (typeof forwarder.enabled !== 'undefined'){
                            data.enabled = forwarder.enabled
                        }
                        if (forwarder.type){
                            data.type = forwarder.type
                        }
                        if (forwarder.server){
                            data.server = forwarder.server
                        }
                        if (forwarder.port){
                            data.port = forwarder.port
                        }
                        if (forwarder.login){
                            data.login = forwarder.login
                        }
                        if (forwarder.password){
                            data.password = forwarder.password
                        }
                        if (forwarder.path){
                            data.path = forwarder.path
                        }
                        if (forwarder.mappings){
                            data.mappings = forwarder.mappings
                        }
                        if (forwarder.newName){
                            data.name = forwarder.newName
                        }
                        if (forwarder.offset){
                            data.offset = forwarder.offset
                        }
                        if (forwarder.header){
                            data.header = forwarder.header
                        }
                        if (forwarder.format){
                            data.format = forwarder.format
                        }
                        if (forwarder.filename) {
                            data.filename = forwarder.filename
                        }
                        if (forwarder.headers && Array.isArray(forwarder.headers)) {
                            data.headers = forwarder.headers
                        }
                        if (forwarder.airport){
                            data.airport = forwarder.airport
                        }
                        data.save((err) => {
                            if (err){
                                if (err.message){
                                    callback(err.message)
                                } else {
                                    callback(err)
                                }
                            } else {
                                callback(null, data)
                            }
                        })
                    }
                })
            }
        } catch (error) {
            callback(error)
        }
    },
    deleteForwarders: function(user, devId, name, callback){
        try {
            if (user.role != "admin"){
                fwds.remove({"user": user.email, "name": name, "sn": devId}, (err) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null)
                    }
                })
            } else {
                fwds.remove({"name": name, "sn": devId}, (err) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null)
                    }
                })
            }
            
        } catch (error) {
            callback(error)
        }
    }
}

module.exports = fwdAPI;
