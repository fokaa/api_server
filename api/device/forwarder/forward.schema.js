/*
 * manage device forwarders
 */

const mongoose = require('mongoose');

const ForwardSchema = new mongoose.Schema({
     // ftp/post
    type: {
        type: String,
        required: true,
        trim: true
    },
    // mail of user cretaing and owning forwarder
    user: {
        type: String,
        required: true,
        trim: true,
    },
    // unique (user only) name of forwarder
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    // enable/disable
    enabled: {
        type: Boolean,
        required: true,
        default: false
    },
    // device serial number used in device registration
    sn:{                 
        type: String,
        required: true,
        trim: true
    },
    // server info login:passowrd@server:port/path/filename
    server:{
        type: String,
        required: true,
        trim: true
    },
    port:{
        type: String,
        required: true,
        trim: true
    },
    path:{
        type: String,
        trim: true
    },
    login:{
        type: String,
        trim: true
    },
    password:{
        type: String,
        trim: true
    },
    filename: {
        type: String,
        trim: true
    },
    // last result and time
    last:{
        type: String
    },
    lasttime:{
        type: Date
    },
    log:[
        {
            _id: false,
            time:{
                type: Date
            },
            result: {
                type: String,
                trim: true
            }
        }
    ],
    mappings:{
        type: String,
        trim: true
    },
    // csv header
    header: {
        type: String,
        trim: true
    },
    //http headers
    headers: [
        {
            name: {
                type: String,
                trim: true  
            },
            value: {
                type: String,
                trim: true  
            }
        }
    ],
    // csv, json, metar
    format: {
        type: String,
        trim: true
    },
    // metar airport code
    airport: {
        type: String,
        trim: true
    },
    // timezone offset 
    offset:{
        type: Number,
        default: 0
    }
});

ForwardSchema.set("timestamps", true);

ForwardSchema.index({ "user": 1, "name": 1}, { unique: true })
ForwardSchema.index({ "user": 1, "type": 1, "server": 1, "port": 1, "path": 1 }, { unique: true })

const forwarder = mongoose.model('forwarder', ForwardSchema);

module.exports = mongoose.model('forwarder');
