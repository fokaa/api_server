/*
 * Core device APIs
 */

const router = require('express').Router()
const bodyParser = require('body-parser')

const jwt = require('../auth/auth.jwt')
const devAPI = require('./device.api')
const fwdAPI = require('./forwarder/forwarder.api')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(jwt.passport.initialize())
router.use(jwt.passport.session())

router.get('/', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.getDevices(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.registerDevice(req.user, req.body, function(reason, data){
        switch(reason){
            case 1:
                res.status(400).json("Not saved");
                break;
            case 2:
                res.status(200).json(data);
                break;
            case 3:
                res.status(404).json("Device not found");
                break;
            case 4:
                res.status(401).json("Device already registered");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/:id/deregister', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.deregisterDevice(req.user, req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(400).json("Not saved");
                break;
            case 2:
                res.status(200).json("Device deregistered");
                break;
            case 3:
                res.status(404).json("Device not found");
                break;
            case 4:
                res.status(403).json("Not authorized");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/:id', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.getDevice(req.user, req.params.id, function(reason, device){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized");
                break;
            case 4:
                res.status(200).json(device);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.put('/:id', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.modifyDevice(req.user, req.params.id, req.body, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized");
                break;
            case 4:
                res.status(200).json("Device modified");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.delete('/:id', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']), function(req, res, next){
    devAPI.deleteDevice(req.params.id, function(reason){
        switch(reason){
        case 1:
            res.status(500).json("Unknown error");
            break;
        case 2:
            res.status(404).json("Device not found");
            break;
        case 3:
            res.status(200).json("Device deleted");
            break;
        default:
            res.status(500).json("Unknown error");
        }
    })
});

router.all('/:id/disable', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.disableDevice(req.user, req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized");
                break;
            case 4:
                res.status(200).json("Device disabled");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.all('/:id/enable', jwt.passport.authenticate('jwt'), function(req, res, next){
    devAPI.enableDevice(req.user, req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized");
                break;
            case 4:
                res.status(200).json("Device enabled");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.all('/:id/assign', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin']), function(req, res, next){
    var id = "", email = ""
    if (req.body.id){
        var id = req.body.id;
    }
    if (req.body.email){
        var id = req.body.email;
    }
    devAPI.assignDevice(req.body.id, req.body.email, req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break
            case 3:
                res.status(200).json("Device assigned");
                break
            case 2:
                res.status(404).json("User not found");
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
})

router.get('/:id/forwarders', jwt.passport.authenticate('jwt'), function(req, res, next){
    fwdAPI.getForwarders(req.user, req.params.id, function(err, data){
        if (err){
            res.status(500).json(err);
        } else {
            res.status(200).json(data)
        }
    })
})

router.get('/:id/forwarder/:name', jwt.passport.authenticate('jwt'), function(req, res, next){
    fwdAPI.getForwarder(req.user, req.params.id, req.params.name, function(err, data){
        if (err){
            res.status(500).json(err);
        } else {
            res.status(200).json(data)
        }
    })
})

router.get('/:id/forwarder/:name/logs', jwt.passport.authenticate('jwt'), function(req, res, next){
    fwdAPI.getForwarderLogs(req.user, req.params.id, req.params.name, function(err, data){
        if (err){
            res.status(500).json(err);
        } else {
            res.status(200).json(data)
        }
    })
})

router.post('/:id/forwarder', jwt.passport.authenticate('jwt'), function(req, res, next){
    if (req.body.forwarder && Object.keys(req.body.forwarder).length > 0){
        fwdAPI.postForwarders(req.user, req.params.id, req.body.forwarder, function(err, data){
            if (err){
                if (err == "Device not found"){
                    res.status(404).json(err);
                } else if (err == "Not permitted") {
                    res.status(401).json(err);
                } else {
                    let msg = "Error saving forwarder"
                    if (err.indexOf('E11000') !== -1) {
                        msg = "Duplicate forwarder error"
                        if (err.indexOf('$name') !== -1) {
                            msg = "Forwarder with this name already exists"
                        }
                    }
                    res.status(500).json(msg);
                }
            } else {
                res.status(200).json(data)
            }
        })
    } else {
        res.status(400).json("Bad request");
    }
})

router.put('/:id/forwarder', jwt.passport.authenticate('jwt'), function(req, res, next){
    if (req.body.forwarder && req.body.forwarder.name){
        fwdAPI.putForwarders(req.user, req.params.id, req.body.forwarder.name, req.body.forwarder, function(err, data){
            if (err){
                let msg = "Error saving forwarder"
                if (err.indexOf('E11000') !== -1) {
                    msg = "Duplicate forwarder error"
                    if (err.indexOf('$name') !== -1) {
                        msg = "Forwarder with this name already exists"
                    }
                }
                res.status(500).json(msg);
            } else {
                res.status(200).json(data)
            }
        })
    } else {
        res.status(400).json("Bad request");
    }
})

router.put('/:id/forwarder/:name', jwt.passport.authenticate('jwt'), function(req, res, next){
    if (req.body.forwarder){
        fwdAPI.putForwarders(req.user, req.params.id, req.params.name, req.body.forwarder, function(err, data){
            if (err){
                let msg = "Error saving forwarder"
                if (err.indexOf('E11000') !== -1) {
                    msg = "Duplicate forwarder error"
                    if (err.indexOf('$name') !== -1) {
                        msg = "Forwarder with this name already exists"
                    }
                }
                res.status(500).json(msg);
            } else {
                res.status(200).json(data)
            }
        })
    } else {
        res.status(400).json("Bad request");
    }
})

router.delete('/:id/forwarder', jwt.passport.authenticate('jwt'), function(req, res, next){
    if (req.body.forwarder && req.body.forwarder.name){
        fwdAPI.deleteForwarders(req.user, req.params.id, req.body.forwarder.name, function(err){
            if (err){
                res.status(500).json(err);
            } else {
                res.status(200).json("Forwarder deleted successfully")
            }
        })
    } else {
        res.status(400).json("Bad request");
    }
})

router.delete('/:id/forwarder/:name', jwt.passport.authenticate('jwt'), function(req, res, next){
    if (req.body.forwarder && req.body.forwarder.name){
        fwdAPI.deleteForwarders(req.user, req.params.id, req.params.name, function(err){
            if (err){
                res.status(500).json(err);
            } else {
                res.status(200).json("Forwarder deleted successfully")
            }
        })
    } else {
        res.status(400).json("Bad request");
    }
})

module.exports = router;
