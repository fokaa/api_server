const mongoose = require('mongoose');

var DevApiSchema = new mongoose.Schema({
    sn:{
        type: String,
        required: true,
        trim: true
    },
    token: {
        type: String,
        required: true,
        trim: true
    },
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    device:{
        type: String,
        required: true,
        trim: true
    },
    type:{
        type: String,
        required: true,
        trim: true
    },
    provider:{
        type: String,
        required: true,
        trim: true
    }
});

DevApiSchema.set("timestamps", true);

var deviceApi = mongoose.model('DeviceApi', DevApiSchema);

module.exports = mongoose.model('DeviceApi');
