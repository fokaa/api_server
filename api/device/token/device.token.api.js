/*
 * Manage access tokens as used by SigFox, Orange or LoraWan backends
 */

const devices = require('../device.schema');
const deviceToken = require('./device.token.schema')
var utils = require('../../utils/helpers');

const deviceTokenApi = {
    addDeviceToken(user, device, callback) {
        if (user.email == "stolcp@gmail.com" || user.email == "andrej.dobak@gmail.com") {
            //should be enabled later
            devices.findOne({"sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    let code = utils.generateRandomString(12)
                    deviceToken.findOne({"user":user._id, "sn": device}, function(err, apiCode) {
                        if (err){
                            callback(1);
                        } else if (!apiCode){
                            apiCode = new deviceToken({
                                "user":user._id,
                                "sn": device, 
                                "token": code, 
                                "device": dev.deviceId,
                                "type": dev.type,
                                "provider": dev.provider
                            });
                        } else {
                            apiCode.token = code;
                        }
                        apiCode.save(function(err){
                            if (err) {
                                console.error("Error saving api code in register " + err);
                                callback(1);
                            } else {
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            }
                        })
                    })
                }
            })
        } else {
            devices.findOne({"owner":user._id, "sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    let code = utils.generateRandomString(12)
                    deviceToken.findOne({"user":user._id, "sn": device}, function(err, apiCode) {
                        if (err){
                            callback(1);
                        } else if (!apiCode){
                            apiCode = new deviceToken({
                                "user":user._id, 
                                "sn": device, 
                                "token": code, 
                                "device": dev.deviceId,
                                "type": dev.type,
                                "provider": dev.provider
                            });
                        } else {
                            apiCode.token = code;
                        }
                        apiCode.save(function(err){
                            if (err) {
                                console.error("Error saving api code in register " + err);
                                callback(1);
                            } else {
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            }
                        })
                    })
                }
            })
        }
    },
    getDeviceToken(user, device, callback) {
        deviceToken.findOne({"user":user._id, "sn": device}, {}, {lean: true}, function(err, apiCode) {
            if (err){
                callback(1);
            } else if (!apiCode){
                callback(2);
            } else {
                callback(3, {sn: apiCode.sn, token: apiCode.token });
            }
        })
    },
    removeDeviceToken(user, device, callback) {
        deviceToken.findOne({"user":user._id, "sn": device}, function(err, apiCode) {
            if (err){
                callback(1);
            } else if (!apiCode){
                callback(2);
            } else {
                apiCode.remove(function(err){
                    if (err){
                        callback(1);
                    } else {
                        callback(3);
                    }
                })
            }
        })
    },
    checkDeviceToken(device, code, callback) {
        deviceToken.find({"sn": device, }, {}, {lean: true}, function(err, apiCode) {
            if (err){
                callback(false);
            } else if (!apiCode){
                callback(false);
            } else {
                apiCode.forEach(api => {
                    if (code == api.token){
                        callback(api);
                        return;
                    }
                })
                callback(false)
            }
        })
    },
    checkDeviceTokenAsync(device, code) {
        return new Promise((resolve, reject) => {
            deviceToken.find({"sn": device}, {}, {lean: true}, function(err, apiCode) {
                if (err){
                    reject(false);
                } else if (!apiCode){
                    reject(false);
                } else {
                    let res = null;
                    for (let ind = 0; ind < apiCode.length; ind++) {
                        if (code == apiCode[ind].token){
                            res = apiCode[ind];
                            break;
                        }
                    }
                    if (res) {
                        resolve(res);
                    } else {
                        reject(false)
                    }
                }
            })
        })
    },
    checkDeviceTokenUser(user, device, code, callback) {
        devices.findOne({"owner":user._id, "sn": device}, {}, {lean: true}, function(err, dev){
            if (err){
                callback(1);
            } else if (!dev){
                callback(2);
            } else {
                deviceToken.findOne({"user":user._id, "sn": device}, {}, {lean: true}, function(err, apiCode) {
                    if (err){
                        callback(1);
                    } else if (!apiCode){
                        callback(2);
                    } else {
                        if (token == apiCode.code){
                            callback(3);
                        } else {
                            callback(4)
                        }
                    }
                })
            }
        })
    }
}

module.exports = deviceTokenApi;

