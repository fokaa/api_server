/*
 * Manage device hash table
 */


const router = require('express').Router();
const bodyParser = require('body-parser');

const hashAPI = require('./device.hash.api');

const jwt = require('../../auth/auth.jwt');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(jwt.passport.initialize());
router.use(jwt.passport.session());

router.post('/', jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin','oper']),function(req, res, next){
    hashAPI.createNew(req.body, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(400).json("Bad data");
                break;
            case 3:
                res.status(200).json("Hash created");
                break;
            case 4:
                res.status(400).json("Duplicate hash");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/',  jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin','oper']),function(req, res, next){
    hashAPI.getHashs(function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(400).json("No data");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.delete('/',  jwt.passport.authenticate('jwt'), jwt.rolesAuth(['admin','oper']),function(req, res, next){
    hashAPI.deleteHash(req.body, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(400).json("Bad data");
                break;
            case 3:
                res.status(200).json("Deleted");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

module.exports = router;
