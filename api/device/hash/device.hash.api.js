/*
 * Manage devices hash table 
 */

const hashDB = require('./device.lookup.schema');

const hashAPI = {
    createNew: (hash, callback) => {
        if (hash.sn && hash.device && hash.provider && hash.type){
            let assoc = {
                sn: hash.sn,
                device: hash.device,
                provider: hash.provider,
                type: hash.type
            };
            let newHash = new hashDB(assoc);
            newHash.save((err) => {
               if (err){
                   if (err.code == 11000){
                       callback(4);
                   }
                   else {
                       callback(1)
                   };
               } else {
                   callback(3);
               }
            });
        } else {
            callback(2);
        }
    },
    getHashs: (callback) => {
        hashDB.find({}, {"__v": 0, "_id": 0}, {lean: true}, (err, data) => {
            if (err) {
                callback(1)
            } else if (!data) {
                callback(2)
            } else {
                callback(3, data);
            }
        })
    },
    deleteHash: (hash, callback) => {
        if (typeof hash.sn != "undefined" &&
            typeof hash.device != "undefined" &&
            typeof hash.provider != "undefined" &&
            typeof hash.type != "undefined") {
            hashDB.findOne({"sn": hash.sn, "device": hash.device, "provider": hash.provider, "type": hash.type}, (err, data) => {
                if (err) {
                    callback(1)
                } else if (!data) {
                    callback(2)
                } else {
                    data.remove(function(err){
                        if (err){
                            callback(1);
                        } else {
                            callback(3);
                        }
                    })
                }
            })
        } else {
            callback(2)
        }
    }
}

module.exports = hashAPI;
