const mongoose = require('mongoose');

let DevHashSchema = new mongoose.Schema({
    //device serial number used in device registration
    sn:{                 
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    //device id sent by operator
    device:{
        type: String,
        required: true,
        trim: true
    },
    //operator identification
    provider:{
        type: String,
        required: true,
        trim: true
    },
    //type simple or extended
    type:{
        type: String,
        required: true,
        trim: true
    },
    //device access token
    token: {
        type: String,
        required: false,
        trim: true
    }
});

DevHashSchema.set("timestamps", true);

DevHashSchema.index({ "device": 1, "provider": 1, "sn": 1, "type": 1 }, { unique: true })
let devHash = mongoose.model('DeviceHashtab', DevHashSchema);

module.exports = mongoose.model('DeviceHashtab');
