/*
 * Secure devices with access codes
 */


const devices = require('../device.schema');
const deviceHash = require('../hash/device.lookup.schema')
var utils = require('../../utils/helpers');

const deviceAuthApi = {
    addDeviceToken(user, device, token, callback) {
        if (user.role == "admin") {
            devices.findOne({"sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    let code = utils.generateRandomString(12)

                    if (typeof token !== "undefined")
                        code = token;
                    deviceHash.findOne({"sn": device}, function(err, apiCode) {
                        if (err || !apiCode){
                            callback(1);
                        } else {
                            apiCode.token = code;
                        }
                        apiCode.save(function(err){
                            if (err) {
                                console.error("Error saving api code in register " + err);
                                callback(1);
                            } else {
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            }
                        })
                    })
                }
            })
        } else {
            devices.findOne({"owner":user._id, "sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    let code = utils.generateRandomString(12)

                    if (typeof token !== "undefined")
                        code = token;
                    deviceHash.findOne({"sn": device}, function(err, apiCode) {
                        if (err || !apiCode){
                            callback(1);
                        } else {
                            apiCode.token = code;
                        }
                        apiCode.save(function(err){
                            if (err) {
                                console.error("Error saving api code in register " + err);
                                callback(1);
                            } else {
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            }
                        })
                    })
                }
            })
        }
    },
    getDeviceToken(user, device, callback) {
        if (user.role == "admin") {
            devices.findOne({"sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    deviceHash.findOne({"sn": device}, function(err, apiCode) {
                        if (err || !apiCode){
                            callback(1);
                        } else {
                            if (apiCode.token)
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            else
                                callback(3, {sn: apiCode.sn, token: "" });
                        }                        
                    })
                }
            })
        } else {
            devices.findOne({"owner":user._id, "sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    deviceHash.findOne({"sn": device}, function(err, apiCode) {
                        if (err || !apiCode){
                            callback(1);
                        } else {
                            if (apiCode.token)
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            else
                                callback(3, {sn: apiCode.sn, token: "" });
                        }
                    })
                }
            })
        }
    },
    removeDeviceToken(user, device, callback) {
        if (user.role == "admin") {
            devices.findOne({"sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    let code = "";
                    deviceHash.findOne({"sn": device}, function(err, apiCode) {
                        if (err || !apiCode){
                            callback(1);
                        } else {
                            apiCode.token = code;
                        }
                        apiCode.save(function(err){
                            if (err) {
                                console.error("Error saving api code in register " + err);
                                callback(1);
                            } else {
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            }
                        })
                    })
                }
            })
        } else {
            devices.findOne({"owner":user._id, "sn": device}, {}, {lean: true}, function(err, dev){
                if (err){
                    callback(1);
                } else if (!dev){
                    callback(2);
                } else {
                    let code = "";
                    deviceHash.findOne({"sn": device}, function(err, apiCode) {
                        if (err || !apiCode){
                            callback(1);
                        } else {
                            apiCode.token = code;
                        }
                        apiCode.save(function(err){
                            if (err) {
                                console.error("Error saving api code in register " + err);
                                callback(1);
                            } else {
                                callback(3, {sn: apiCode.sn, token: apiCode.token });
                            }
                        })
                    })
                }
            })
        }
    },
    getAllOwned: async function(user, callback) {
        var myTokens = [];
        try {
            let myDev = await devices.find({ "owner": user._id }, {"sn": 1}, {lean: true})
            for (let i = 0; i < myDev.length; i++) {
                let token = await deviceHash.findOne({ "sn": myDev[i].sn }, {"token": 1, "sn": 1, "_id": 0}, {lean: true});
                if (token) {
                    if (!token.token)
                        token.token = "";
                    myTokens.push(token);
                }
            }
            callback(3, myTokens);
        } catch (error) {
            callback(1); 
        }
    },
    addToAllOwned: async function(user, token, callback) {
        let code = utils.generateRandomString(12)
        if (typeof token !== "undefined")
            code = token;
        try {
            let myDev = await devices.find({ "owner": user._id }, {"sn": 1}, {lean: true});
            myDev.forEach(async function(dev) {
                await deviceHash.update({ "sn": dev.sn },{ "$set": { "token": code } }, { safe: true, multi:true })
            })
            callback(3, { "token": code } );
        } catch (error) {
            callback(1); 
        }
    },
    deleteFromAllOwned: async function(user, callback) {
        let code = "";
        try {
            let myDev = await devices.find({ "owner": user._id }, {"sn": 1}, {lean: true});
            myDev.forEach(async function(dev) {
                await deviceHash.update({ "sn": dev.sn },{ "$set": { "token": code } }, { safe: true, multi:true })
            })
            callback(3, { "token": code } );
        } catch (error) {
            callback(1); 
        }
    }
}

module.exports = deviceAuthApi;
