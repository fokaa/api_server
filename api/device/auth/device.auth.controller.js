/*
 * Device access codes
 */


const router = require('express').Router()
const bodyParser = require('body-parser')

const jwt = require('../../auth/auth.jwt')

const deviceApi = require('./device.auth.api')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(jwt.passport.initialize())
router.use(jwt.passport.session())

router.get('/owned', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.getAllOwned(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.get('/:id', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.getDeviceToken(req.user, req.params.id, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.post('/owned', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.addToAllOwned(req.user, req.body.token, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.put('/owned', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.addToAllOwned(req.user, req.body.token, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.delete('/owned', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.deleteFromAllOwned(req.user, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});
router.post('/:id', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.addDeviceToken(req.user, req.params.id, req.body.token, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.put('/:id', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.addDeviceToken(req.user, req.params.id, req.body.token, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json(data);
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

router.delete('/:id', jwt.passport.authenticate('jwt'), function(req, res, next){
    deviceApi.removeDeviceToken(req.user, req.params.id, function(reason){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Not found");
                break;
            case 3:
                res.status(200).json("Deleted");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    })
});

module.exports = router;
