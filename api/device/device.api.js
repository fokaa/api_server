/* 
 * Core device APIs
 */

const devices = require('./device.schema');
const users = require('../user/user.schema');
const weathers = require('../weather/weather.schema');
const hashs = require('./hash/device.lookup.schema');
const forwarders = require('./forwarder/forward.schema');
const tokens = require('./token/device.token.schema');
const userAPI = require('../user/user.api');
const helpers = require('../utils/helpers')

const deviceAPI = {
    getDevice: function(user, devId, callback){
        if (user && user._id && devId){
           devices.findOne({"sn": devId}, {}, {lean: true}, function(err, device){
                if (err) {
                    callback(1);
                } else if (!device){
                    callback(2);
                } else if ((device.owner != user._id) && (!device.shared) && (user.role != 'admin')){
                    callback(3);
                } else {
                    if (device.position && device.position.coordinates && Array.isArray(device.position.coordinates) && device.position.coordinates.length === 2) {
                        let posun = device.position.coordinates[0]
                        device.position.coordinates[0] = device.position.coordinates[1]
                        device.position.coordinates[1] = posun
                    }
                    device.next24 = [];
                    device.fc48 = [];
                    if (device.forecast && device.forecast !== ""){
                        weathers.findById(device.forecast,{}, {lean: true}, function(err, data){
                            if (err || !data){
                                delete device.forecast;
                                callback(4, device);        
                            } else {
                                device.next24 = data.next24;
                                if (data.next24 && data.last24) {
                                    device.fc48 = data.last24.concat(data.next24)
                                }
                                delete device.forecast;
                                callback(4, device);        
                            }
                        })
                    }
                    else {
                        delete device.forecast;
                        callback(4, device);
                    }
                }
            })
        } else {
            callback(1);
        }
    },
    getDevices: function(user, callback){
        if (user && user.role && user._id){
            switch(user.role){
                case 'user':
                    devices.find({"owner": user._id}, {"__v":0, "comments": 0}, {lean: true},   function(err, data){
                        if (err){
                            callback(1);
                        } else if (data.length == 0){
                            callback(3,[]);
                        } else {
                            data.forEach((dev) => {
                                if ((dev.position) && (dev.position.coordinates)){
                                    var lat = dev.position.coordinates[1];
                                    var lng = dev.position.coordinates[0];
                                    dev.position = [lat, lng];
                                }
                            });
                            callback(3, data);
                        }
                    });
                    break;
                case 'admin':
		case 'oper':
                    devices.find({}, {"__v":0}, {lean: true}, function(err, data){
                        if (err){
                            callback(1);
                        } else if (data.length == 0){
                            callback(3,[]);
                        } else {
                            data.forEach((dev) => {
                                    if ((dev.position) && (dev.position.coordinates)){
                                    var lat = dev.position.coordinates[1];
                                    var lng = dev.position.coordinates[0];
                                    dev.position = [lat, lng];
                                }
                            });
                            callback(3, data);
                        }
                    });
                    break;
                default:
                    callback(1);
            }
        } else {
            callback(1)
        }
    },
    registerDevice: function(user, devData, callback){
        var dev = {};
        try {
            dev.sn = devData.sn;
            dev.type = devData.type;
            dev.owner = user._id;
            if ("name" in devData){
                dev.name = devData.name;
            }
            if (("position" in devData) && devData.position.length == 2){
                dev.position = devData.position;
            }
            if ("elevation" in devData){
                dev.elevation = devData.elevation;
            }
            if ("height" in devData){
                dev.height = devData.height;
            }
            if ("shared" in devData){
                dev.shared = devData.shared;
            }
            if (("tags" in devData) && (Array.isArray(devData.tags))){
                dev.tags = devData.tags;
            }
            if ("settings" in devData){
                dev.settings = devData.settings;
            }
        }
        catch (err){
            callback(1);
            return;
        }
        hashs.findOne({"sn":devData.sn}, {}, {lean: true},  (err, hash) => {
            if (err){
                callback(1);
                console.error("Cannot search for hash in device register")
            } else if (!hash){
                callback(3);
            } else {
                dev.type = hash.type;
                dev.provider = hash.provider;
                dev.deviceId = hash.device;
                var device = new devices(dev);
                device.save(function(err){
                    if (err){
                        if (err.code == 11000){
                            callback(4);
                        } else {
                            console.error("Error saving device in register " + err);
                            callback(1);
                        }
                    } else {
                        callback(2, device);
                    }
                })
            }
        })
    },
    deregisterDevice: async function(user, sn, callback){
        var dev = "";
        try {
            users.findOne({"email" : user.email}, {}, {lean: true},  function(err, login){
                if (err){
                    callback(1);
                } else if (!login){
                    callback(2);
                } else {
                    devices.findOne({"sn":sn}, async function(err, data){
                        if (err){
                            callback(1);
                        } else if (!data){
                            callback(2);
                        } else {
                            if (user.role === 'admin' || data.owner.equals(login._id)){
                                await deviceAPI.deleteDeviceDependents(sn);
                                await devices.remove({"sn": sn})
                                callback(2)
                            } else {
                                callback(4)
                            }
                        }
                    })
                }
            })
        }
        catch (err){
            callback(1);
            return;
        }
    },
    deleteDeviceDependents: function(sn) {
        return new Promise(async function(resolve, reject){
            try {
                await users.update({"favorites.sn": sn},{ "$pull": { "favorites": { "sn": sn } }}, { safe: true, multi:true })
                await forwarders.deleteMany({"sn": sn});
                await tokens.deleteMany({"sn": sn});    
                resolve(sn);
            } catch (error) {
                reject(sn);
            }
        })
    },
    assignDevice: function(userId, email, deviceSN, callback){
        if (deviceSN && !userId && email){
            users.findOne({"email" : email}, {}, {lean: true},  function(err, login){
                if (err){
                    callback(1);
                } else if (!login){
                    callback(2);
                } else {
                    devices.findOne({"sn":deviceSN}, function(err, data){
                        if (err){
                            callback(1);
                        } else if (!data){
                            callback(2);
                        } else {
                            data.owner = data._id;
                            data.save(function(err){
                                if (err){
                                    console.error("Error saving device in assignDevice " + err);
                                } else {
                                    callback(3);
                                }
                            })
                        }
                    })
                }
            })
        } else if (userId && deviceSN){
            devices.findOne({"sn":deviceSN}, function(err, data){
                if (err){
                    callback(1);
                } else if (!data){
                    callback(2);
                } else {
                    data.owner = userId;
                    data.save(function(err){
                        if (err){
                            console.error("Error saving device in assignDevice " + err);
                        } else {
                            callback(3);
                        }
                    })
                }
            })
        } else {
            callback(1);
        }
    },
    modifyDevice: function(user, sn, device, callback){
        devices.findOne({"sn": sn}, function(err, dev){
            if (err){
                callback(1);
            } else if (!dev){
                callback(2);
            } else {
                if ((dev.owner != user._id) && (user.role != 'admin')){
                    callback(3);
                    return;
                }
                if ("name" in device){
                    dev.name = device.name;
                }
                if (("position" in device) && device.position.length == 2){
                    dev.position = device.position;
                }
                if ("elevation" in device){
                    dev.elevation = device.elevation;
                }
                if ("height" in device){
                    dev.height = device.height;
                }
                if ("shared" in device){
                    dev.shared = device.shared;
                }
                if (("tags" in device) && (Array.isArray(device.tags))){
                    dev.tags = device.tags;
                }
                if ("settings" in device){
                    dev.settings = helpers.deepUpdate(dev.settings, device.settings);
                    dev.markModified("settings")
                }
                dev.save(function(err){
                    if (err){
                        callback(1);
                    } else {
                        callback(4);
                    }
                })
            }
        })
    },
    enableDevice: function (user, id, callback) {
        if (id && user._id && user.role){
            devices.findById(id, function(err, device){
                if (err) {
                    callback(1);
                } else if (!device){
                    callback(2);
                } else {
                    if ((user.role == 'admin') || (user._id == device.owner)){
                        device.disabled = false;
                        device.save(function(err){
                            if (err){
                                callback(1);
                            } else {
                                callback(4);
                            }
                        })
                    } else {
                        callback(3);
                    }
                }
            })
        } else {
            callback(1);
        }
    },
    disableDevice: function (user, id, callback) {
        if (id && user._id && user.role){
            devices.findById(id, function(err, device){
                if (err) {
                    callback(1);
                } else if (!device){
                    callback(2);
                } else {
                    if ((user.role == 'admin') || (user._id == device.owner)){
                        device.disabled = true;
                        device.save(function(err){
                            if (err){
                                callback(1);
                            } else {
                                callback(4);
                            }
                        })
                    } else {
                        callback(3);
                    }
                }
            })
        } else {
            callback(1);
        }
    },
    deleteDevice: function(id, callback){
        if (id){
            devices.findById(id, async function(err, device){
                if (err){
                    callback(1);
                } else if (!device){
                    callback(2);
                } else {
                    try {
                        await deviceAPI.deleteDeviceDependents(device.sn);
                        device.remove(function(err){
                            if (err){
                                callback(1);
                            } else {
                                callback(3);
                            }
                        })
                    } catch (error) {
                        callback(1);
                    }
                }
            })
        } else {
            callback(1);
        }
    },
    getSharedDevices: function(user, callback){
        devices.find({"shared": true, "disabled":false, "owner":{"$ne": user._id} }, {"__v":0, "disabled": 0, "comments": 0, "tags": 0}, {lean: true})
            .populate("owner",{"username":1, "_id": 0, "picture": 1})
            .exec(function(err, data){
            if (err){
                callback(1);
            } else if (!data || data.length == 0){
                callback(2);
            } else {
                data.forEach((dev) => {
                    if ((dev.position) && (dev.position.coordinates)){
                        var lat = dev.position.coordinates[1];
                        var lng = dev.position.coordinates[0];
                        dev.position.coordinates = [lat, lng];
                    }
                });
                callback(3, data);
            }
        });
    },
    getAllDevices: function(user, callback){
        devices.find({"$or":[{"owner": user._id}, {"shared": true}], "disabled": false}, {"__v":0, "disabled": 0, "comments": 0, "tags": 0}, {lean: true})
            .populate("owner",{"username":1, "_id": 0, "picture": 1})
            .exec(function(err, data){
            if (err){
                callback(1);
            } else if (!data || data.length == 0){
                callback(2);
            } else {
                data.forEach((dev) => {
                    if ((dev.position) && (dev.position.coordinates)){
                        var lat = dev.position.coordinates[1];
                        var lng = dev.position.coordinates[0];
                        dev.position.coordinates = [lat, lng];
                    }
                });
                callback(3, data);
            }
            });
    },
    getFavoriteDevices: function(user, callback){
        userAPI.getUserFavorites(user._id, function(reason, favorites){
            if (reason == 2){
                if (favorites.length == 0){
                    callback(3, favorites);
                } else {
                    var query = [];
                    favorites.forEach((fav)=>{
                        query.push({"sn":fav.sn});
                    })
                    devices.find({"$or": query},{"_id": 0, "__v": 0, "comments": 0, "tags": 0, "shared": 0, "disabled": 0}, {lean: true})
                        .populate("owner",{"username":1, "_id": 0, "picture": 1})
                        .exec( function(err, data){
                        if (err){
                            callback(1);
                        } else if (!data || data.length == 0){
                            callback(2);
                        } else {
                            data.forEach((dev) => {
                                if ((dev.position) && (dev.position.coordinates)){
                                    var lat = dev.position.coordinates[1];
                                    var lng = dev.position.coordinates[0];
                                    dev.position.coordinates = [lat, lng];
                                }
                            });
                            callback(3, data);
                        }
                    })
                }
            } else {
                if (reason == 1){
                    callback(reason);
                } else {
                    callback(2);
                }

            }
        })
    }
}

module.exports = deviceAPI;
