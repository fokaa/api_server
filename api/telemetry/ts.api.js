/* 
 * InfluxDB telemetry APIs
 */

const devAPI = require('../device/device.api');
const influx = require('../../db/influx');
const devHash = require('../device/hash/device.lookup.schema');

const tsAPI = {
    getLastValues: function(user, station, measurements, callback) {
        if (station){
            devAPI.getDevice(user, station, function(reason, device){
                if (reason != 4){
                    callback(reason);
                } else {
                    if (Array.isArray(measurements)){
                        var measur = "";
                        measurements.forEach(function(m){
                            if (measur == ""){
                                measur += m;
                            } else {
                                measur += "," +m;
                            }
                        });
                        //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                        var query = "SELECT "+ measur + " from " + device.type + " where (device = '"+ device.deviceId +"' and provider = '"
                            + device.provider + "') order by time desc limit 1";
                    } else {
                        var query = "SELECT * from " + device.type + " where (device = '"+ device.deviceId +"' and provider = '"
                            + device.provider + "') order by time desc limit 1";
                    }
                    influx.query(query).then(function(result){
                        if (result && result[0]){
                            result[0].time = result[0].time.valueOf()
                            callback(4, result);
                        } else {
                            callback(1);
                        }
                    }).catch(function(err){
                        callback(1);
                    })
                }
            })
        } else {
            callback(1);
        }
    },
    getTimeSeries: function(user, station, from, to, aggregation, measurements, method, callback) {
        if (station){
            devAPI.getDevice(user, station, function(reason, device){
                if (reason != 4){
                    callback(reason);
                } else {
                    station = device.deviceId;
                    //SELECT mean(*) from mqtt_consumer where "station" = 'weatherBit' and time <= now() and time >= now() - 1d group by time(10h) fill(none)
                    var query = "SELECT ";
                    if (Array.isArray(measurements)) {
                        var measur = "";
                        measurements.forEach(function (m) {
                            if (query == "SELECT ") {
                                query += method+"(" + m + ")";
                            } else {
                                query += "," + method+"(" + m + ")";
                            }
                        });
                    } else {
                        query += method+"(*)"
                    }

                    query +=  " from " + device.type + " where (device = '"+ device.deviceId +"' and provider = '"
                        + device.provider + "') ";

                    //a 11788247
                    //ms 11788247000
                    //ns 11788247000000
                    if (from) {
                        from = parseInt(from, 10);
                        if (from < 946684861000){
                            from *= 1000000000;
                        }
                        if (from < 946684861000000000){
                            from *= 1000000;
                        }
                        query += "AND time >= " + from + " ";
                    }

                    if (to) {
                        to = parseInt(to,10);
                        if (to < 946684861000){
                            //assume seconds
                            to *= 1000000000 ;
                        }
                        if (to < 946684861000000000) {
                            //assume milliseconds
                            to *= 1000000;
                        }
                        query += "AND time <= " + to + " ";
                    }

                    //group by time(10h) fill(none)
                    if (aggregation){
                        var index = aggregation.indexOf('M'),
                            kolko = 0;
                        if (index != -1){
                            kolko = Number.parseInt(aggregation.slice(0,index), 10) * 31;
                            aggregation = kolko + 'd';
                        } else {
                            index = aggregation.indexOf('Y');
                            if (index != -1){
                                kolko = Number.parseInt(aggregation.slice(0,index), 10) * 31;
                                aggregation = kolko + 'd';
                            }
                        }
                        query += "group by time(" + aggregation + ") fill(none)"
                    }

                    influx.query(query).then(function(results){
                        if (results){
                            var arr = [];

                            if (Array.isArray(measurements)){
                                for (var y = 0; y < results.length; y++){
                                    var res = {};
                                    var z = 0;
                                    for (var x in results[y]){
                                        if (x == "time"){
                                            res[x] = results[y][x].valueOf();
                                        } else {
                                            res[measurements[z]] = results[y][x];
                                            z++;
                                        }
                                    }
                                    arr.push(res);
                                }
                            } else {
                                for (var y = 0; y < results.length; y++){
                                    var res = {};
                                    for (var x in results[y]){
                                        if (x == "time"){
                                            res[x] = results[y][x].valueOf();
                                        } else {
                                            res[x.substring(method.length + 1)] = results[y][x];
                                        }
                                    }
                                    arr.push(res);
                                }
                            }

                            callback(4, arr);
                        }
                    }).catch(function(err){
                        callback(5);
                    })
                }
            })
        } else {
            callback(1);
        }
    },
    getLastOverview: function(stations, measurements, callback) {
        if (Array.isArray(stations) && (stations.length > 0)){
            if (Array.isArray(measurements)){
                var measur = "";
                measurements.forEach(function(m){
                    if (measur == ""){
                        measur += m;
                    } else {
                        measur += "," +m;
                    }
                });
                //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                var querybase = "SELECT "+ measur + " from ";
            } else {
                var querybase = "SELECT * from ";
            }
            let stattypes = []
            let x, y;
            for (x = 0; x < stations.length; x++){
                let found = false
                for (y = 0; y < stattypes.length; y++){
                    if (stattypes[y].type == stations[x].type) {
                        stattypes[y].stations.push(stations[x])
                        found = true
                    }
                }
                if (!found) {
                    stattypes.push({
                        type: stations[x].type,
                        stations: [stations[x]]
                    })
                }
            }
            let query = "", querylist = [];
            for (y = 0; y < stattypes.length; y++){
                query = querybase
                let stat = "";
                for (x = 0; x < stattypes[y].stations.length; x++){
                    if (x != 0){
                        stat += " or "
                    } else {
                        query += " " + stattypes[y].type + " where ";
                    }
                    stat += "(device = '"+ stattypes[y].stations[x].deviceId +"' and provider = '" + stattypes[y].stations[x].provider + "')";
                }
                stat = "(" + stat + ")";
                query = query + stat + "  group by device order by time desc limit 1"    
                querylist.push(query)
            }
            
            influx.query(querylist).then(function(res){
                if (Array.isArray(res)){
                    for (let z = 0; z < res.length; z++) {
                        if (Array.isArray(res[z])){
                            let result = res[z]
                            for (let y = 0; y < result.length; y++){
                                for (let x = 0; x < stations.length; x++){
                                    if (result[y].device == stations[x].deviceId){
                                        stations[x].last = result[y];
                                        delete stations[x].last.provider;
                                        delete stations[x].last.device;
                                        stations[x].last.time = stations[x].last.time.valueOf();
                                        break;
                                    }
                                }
                            }
                        } else {
                            let result = res
                            for (let y = 0; y < result.length; y++){
                                for (let x = 0; x < stations.length; x++){
                                    if (result[y].device == stations[x].deviceId){
                                        stations[x].last = result[y];
                                        delete stations[x].last.provider;
                                        delete stations[x].last.device;
                                        stations[x].last.time = stations[x].last.time.valueOf();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    //SELECT mean(temperature) from basic where (device = 'bl6-369A79' and provider = 'sigfox' and time >= now() - 24h) group by time(3h) order by time desc
                    callback(4, stations);
                } else {
                    callback(4, stations);
                }
            }).catch(function(err){
                console.error("Bad influx call: ", query, err)
                callback(1);
            })
        } else if (stations && stations.deviceId && (stations.type === "open" || stations.type === "basic")) {
            var measur = "";
            if (Array.isArray(measurements)){
                measurements.forEach(function(m){
                    if (measur == ""){
                        measur += m;
                    } else {
                        measur += "," +m;
                    }
                });
                //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                var startq = "SELECT "+ measur + " from "  + stations.type + " where ";
            } else {
                var startq = "SELECT * from " + stations.type + " where ";
            }
            var query = startq;
            let stat = "";
            stat += "(device = '"+ stations.deviceId +"' and provider = '" + stations.provider + "')";
            query = query + stat + " group by device order by time desc limit 1"
            influx.query(query).then(function(result){
                if (Array.isArray(result)){
                    for (let y = 0; y < result.length; y++){
                        stations.last = result[y];
                        delete stations.last.provider;
                        delete stations.last.device;
                        stations.last.time = stations.last.time.valueOf();
                    }   
                    measur = "";
                    if (Array.isArray(measurements)){
                        measurements.forEach(function(m){
                            if (measur == ""){
                                measur += " first(" + m + ")";
                            } else {
                                measur += "," +" first(" + m + ")";
                            }
                        });
                        //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                        startq = "SELECT "+ measur + " from "  + stations.type + " where ";
                    } else {
                        startq = "SELECT first(*), sum(rain) as first_sumrain from " + stations.type + " where ";
                    }

                    query = startq + "(device = '"+ stations.deviceId +"' and provider = '" + stations.provider + "' and time >= now() - 30h) group by time(3h) fill(none) order by time desc limit 9"
                    influx.query(query).then(function(results){
                        var arr = [];
                        for (var y = 0; y < results.length; y++){
                            var res = {};
                            for (var x in results[y]){
                                if (x == "time"){
                                    res[x] = results[y][x].valueOf();
                                } else {
                                    var label;
                                    if (Array.isArray(measurements)){
                                        if (x.indexOf("first") != -1){
                                            label = x.substring("first".length + 1);
                                            if (label == ""){
                                                label = measurements[0];
                                            } else {
                                                label = measurements[parseInt(label)]
                                            }
                                            res[label] = results[y][x];
                                        }
                                    } else {
                                        label = x.substring("first".length + 1);
                                        if (label !== "rain") {
                                            if (label === "sumrain")
                                                label = "rain"
                                            res[label] = results[y][x]
                                        }
                                    }
                                }
                            }
                            arr.push(res);
                        }
                        stations.last24 = arr;
                        //get max and min
                        measur = "";
                        if (Array.isArray(measurements)){
                            measurements.forEach(function(m){
                                if (measur == ""){
                                    measur += " max(" + m + ")" + " min(" + m + ")";
                                } else {
                                    measur += "," + " max(" + m + ")" + " min(" + m + ")";
                                }
                            });
                            //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                            startq = "SELECT "+ measur + " from "  + stations.type + " where ";
                        } else {
                            startq = "SELECT max(*), min(*) from " + stations.type + " where ";
                        }
                        
                        query = startq + "(device = '"+ stations.deviceId +"' and provider = '" + stations.provider + "' and time <= now() and time >= now()-24h)"
                        influx.query(query).then(function(results){
                            let max = {}, min = {};
                            if (results.length > 0){
                                for (let x in results[0]){
                                    if (x.indexOf("max") != -1){
                                        max[x.substring("max".length + 1)] = results[0][x];
                                    } else if (x.indexOf("min") != -1){
                                        min[x.substring("min".length + 1)] = results[0][x];
                                    }
                                }
                            }
                            stations.max = max;
                            stations.min = min;
                            callback(4, stations);  
                        }).catch(function(err){
                            console.error("Bad influx call: ", query, err);
                            callback(1);
                        })
                    }). catch(function(err){
                        console.error("Bad influx call: ", query, err)
                        callback(1);
                    })
                } else {
                    callback(4,stations);
                }
            }).catch(function(err){
                console.error("Bad influx call: ", query, err)
                callback(1);
            })
        } else if (stations && stations.deviceId && stations.type === "raw") {
            this.getRawOverview(stations, measurements).then((station) => {
                if (station) {
                    callback(4,station);
                } else {
                    callback(2)    
                }
            })
        } else {
            callback(2);
        }
    },
    getRawOverview: async function(stations, measurements) {
        var measur = "";
        var max = {}, min = {};
        if (Array.isArray(measurements)){
            measurements.forEach(function(m){
                if (measur == ""){
                    measur += m;
                } else {
                    measur += "," +m;
                }
            });
            var startq = "SELECT "+ measur + " from "  + stations.type + " where ";
        } else {
            var startq = "SELECT * from " + stations.type + " where ";
        }
        var query = startq;
        let stat = "";
        stat += "(device = '"+ stations.deviceId +"' and provider = '" + stations.provider + "')";
        query = query + stat + " group by device order by time desc limit 1";
        try {
            var last = await influx.query(query);
            if (last.length > 0) {
                query = "SELECT last(*) from " + stations.type + " where " + stat;
                var sensors = await influx.query(query);
                if (sensors.length > 0) {
                    for (sens in sensors[0]) {
                        if (sens !== "time" && sens.slice(sens.indexOf("last_") + "last_".length, sens.length).indexOf("_") === -1) {
                            last[0][sens.slice(sens.indexOf("last_") + "last_".length, sens.length)] = sensors[0][sens];
                        }
                    }
                }
                for (sens in last[0]) {
                    if (!last[0][sens]) {
                        delete last[0][sens];
                    }
                }
                stations.last = last[0];
                delete stations.last.provider;
                delete stations.last.device;
                stations.last.time = stations.last.time.valueOf();
                measur = "";
                if (Array.isArray(measurements)){
                    measurements.forEach(function(m){
                        if (measur == ""){
                            measur += " first(" + m + ")";
                        } else {
                            measur += "," +" first(" + m + ")";
                        }
                    });
                    //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                    startq = "SELECT "+ measur + " from "  + stations.type + " where ";
                } else {
                    startq = "SELECT first(*), sum(rain) as first_sumrain from " + stations.type + " where ";
                }

                query = startq + "(device = '"+ stations.deviceId +"' and provider = '" + stations.provider + "' and time >= now() - 30h) group by time(3h) fill(none) order by time desc limit 9"
                var results = await influx.query(query);
                var arr = [];
                for (var y = 0; y < results.length; y++){
                    var res = {};
                    for (var x in results[y]){
                        if (x == "time"){
                            res[x] = results[y][x].valueOf();
                        } else {
                            var label;
                            if (Array.isArray(measurements)){
                                if (x.indexOf("first") != -1){
                                    label = x.substring("first".length + 1);
                                    if (label == ""){
                                        label = measurements[0];
                                    } else {
                                        label = measurements[parseInt(label)]
                                    }
                                    res[label] = results[y][x];
                                }
                            } else {
                                label = x.substring("first".length + 1);
                                if (label !== "rain") {
                                    if (label === "sumrain")
                                        label = "rain"
                                    res[label] = results[y][x]
                                }
                            }
                        }
                    }
                    arr.push(res);
                }
                stations.last24 = arr;
                measur = "";
                if (Array.isArray(measurements)){
                    measurements.forEach(function(m){
                        if (measur == ""){
                            measur += " max(" + m + ")" + " min(" + m + ")";
                        } else {
                            measur += "," + " max(" + m + ")" + " min(" + m + ")";
                        }
                    });
                    //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                    startq = "SELECT "+ measur + " from "  + stations.type + " where ";
                } else {
                    startq = "SELECT max(*), min(*) from " + stations.type + " where ";
                }
                
                query = startq + "(device = '"+ stations.deviceId +"' and provider = '" + stations.provider + "' and time <= now() and time >= now()-24h)"
                results = await influx.query(query);
                if (results.length > 0){
                    for (let x in results[0]){
                        if (x.indexOf("max") != -1){
                            max[x.substring("max".length + 1)] = results[0][x];
                        } else if (x.indexOf("min") != -1){
                            min[x.substring("min".length + 1)] = results[0][x];
                        }
                    }
                }
                stations.max = max;
                stations.min = min;
                return stations;
            }
        } catch (error) {
            return null;
        }
        return null;
    },
    getLastOverviewOne: function(stations, measurements, callback) {
        if (Array.isArray(stations) && (stations.length > 0)){
            if (Array.isArray(measurements)){
                var measur = "";
                measurements.forEach(function(m){
                    if (measur == ""){
                        measur += m;
                    } else {
                        measur += "," +m;
                    }
                });
                //select * from basic where (device='bl6-369A79' and provider='sigfox') order by time desc limit 1
                var query = "SELECT "+ measur + " from ";
            } else {
                var query = "SELECT * from ";
            }
            let stat = "";
            for (let x = 0; x < stations.length; x++){
                if (x != 0){
                    stat += " or "
                } else {
                    query += " " + stations[x].type + " where ";
                }
                stat += "(device = '"+ stations[x].deviceId +"' and provider = '" + stations[x].provider + "')";
            }
            stat = "(" + stat + ")";
            query = query + stat + "  group by device order by time desc limit 1"
            influx.query(query).then(function(result){
                if (Array.isArray(result)){
                    for (let y = 0; y < result.length; y++){
                        for (let x = 0; x < stations.length; x++){
                            if (result[y].device == stations[x].deviceId){
                                stations[x].last = result[y];
                                delete stations[x].last.provider;
                                delete stations[x].last.device;
                                stations[x].last.time = stations[x].last.time.valueOf();
                                break;
                            }
                        }
                    }
                    callback(4, stations);
                } else {
                    callback(1);
                }
            }).catch(function(err){
                callback(1);
            })
        } else {
            callback(1);
        }
    },
    //select temperature from basic where device='bl6-370674' and  time >= 1529222421000000000 and time <= 1529227822000000000
    getTable: function(user, station, from, to, measurements,  type, cumulative, week, callback) {
            if (station){
                devAPI.getDevice(user, station, function(reason, device){
                    if (reason != 4){
                        callback(reason);
                    } else {
                        station = device.deviceId;
                        //SELECT mean(*) from mqtt_consumer where "station" = 'weatherBit' and time <= now() and time >= now() - 1d group by time(10h) fill(none)
                        var query = "SELECT ";
                        if (Array.isArray(measurements)) {
                            var measur = "";
                            measurements.forEach(function (m) {
                                if (query == "SELECT ") {
                                    query +=  m;
                                } else {
                                    query += "," + m ;
                                }
                            });
                        } else {
                            query += "*"
                        }

                        query +=  " from " + device.type + " where (device = '"+ device.deviceId +"' and provider = '"
                            + device.provider + "') ";

                        let backupQuery = query;

                        //a 11788247
                        //ms 11788247000
                        //ns 11788247000000
                        if (from) {
                            from = parseInt(from, 10);
                            if (from < 946684861000){
                                from *= 1000000000;
                            }
                            if (from < 946684861000000000){
                                from *= 1000000;
                            }
                            query += "AND time >= " + from + " ";
                        }

                        if (to) {
                            to = parseInt(to,10);
                            if (to < 946684861000){
                                //assume seconds
                                to *= 1000000000 ;
                            }
                            if (to < 946684861000000000) {
                                //assume milliseconds
                                to *= 1000000;
                            }
                            query += "AND time <= " + to + " ";
                        }

                        influx.query(query).then(function(results){
                            if (results){
                                var arr = [];

                                if (Array.isArray(measurements)){
                                    for (var y = 0; y < results.length; y++){
                                        var res = {};
                                        var z = 0;
                                        for (var x in results[y]){
                                            if (x == "time"){
                                                res[x] = results[y][x].valueOf();
                                            } else {
                                                res[measurements[z]] = results[y][x];
                                                z++;
                                            }
                                        }
                                        arr.push(res);
                                    }
                                } else {
                                    for (var y = 0; y < results.length; y++){
                                        var res = {};
                                        for (var x in results[y]){
                                            if (x == "device" || x == "provider"){
                                                continue;
                                            } else if (x == "time"){
                                                res[x] = results[y][x].valueOf();
                                            } else {
                                                res[x.substring()] = results[y][x];
                                            }
                                        }
                                        arr.push(res);
                                    }
                                }

                                let start = new Date(from/1000000);
                                
                                if (typeof arr[0] != 'undefined'&& typeof arr[0].rain != 'undefined' && type == "time" && cumulative != 'd'){
                                
                                    if (cumulative == "m"){
                                        start = new Date(start.getFullYear(), start.getMonth()).valueOf();
                                    } else {
                                        if (week == "week"){
                                            start = new Date(start.getFullYear(), start.getMonth(),start.getDate()-start.getDay()).valueOf();
                                        } else {
                                            start = new Date(start.getFullYear(), start.getMonth(),start.getDate()-start.getDay()+1).valueOf();
                                        }
                                    }
                                    start *= 1000000;
                                    
                                    backupQuery += "AND time >= " + start + " " + "AND time < " + from + " ";
                                    backupQuery = backupQuery.replace("*", "sum(rain)");

                                    influx.query(backupQuery).then(function(results){
                                        if (results && typeof results[0] != 'undefinced' && typeof results[0].sum != 'undefinced'){
                                            arr[0].rainC = results[0].sum;
                                        }
                                        callback(4, arr);
                                    }).catch(function(err){
                                        callback(4, arr);
                                    })
                                } else {
                                    callback(4, arr);
                                }
                            }
                        }).catch(function(err){
                            callback(5);
                        })
                    }
                })
            } else {
                callback(1);
            }
    },
    getExternalApiData: function( device, from, to, measurements, type, cumulative, week, callback) {
        //SELECT mean(*) from mqtt_consumer where "station" = 'weatherBit' and time <= now() and time >= now() - 1d group by time(10h) fill(none)
        var query = "SELECT ";
        if (!measurements) {
            measurements = ["temperature", "co2","dewPoint","humidity","irradiation","pressure","rain","rain_tip"];
        }
        if (Array.isArray(measurements)) {
            var measur = "";
            measurements.forEach(function (m) {
                if (query == "SELECT ") {
                    query +=  m;
                } else {
                    query += "," + m ;
                }
            });
        } else {
            query += "*"
        }

        query +=  " from " + device.type + " where (device = '"+ device.device +"' and provider = '"
            + device.provider + "') ";

        let backupQuery = query;

        //a 11788247
        //ms 11788247000
        //ns 11788247000000
        if (from) {
            from = parseInt(from, 10);
            if (from < 946684861000){
                from *= 1000000000;
            }
            if (from < 946684861000000000){
                from *= 1000000;
            }
            query += "AND time >= " + from + " ";
        }

        if (to) {
            to = parseInt(to,10);
            if (to < 946684861000){
                //assume seconds
                to *= 1000000000 ;
            }
            if (to < 946684861000000000) {
                //assume milliseconds
                to *= 1000000;
            }
            query += "AND time <= " + to + " ";
        }
        influx.query(query).then(function(results){
            if (results){
                var arr = [];
                if (Array.isArray(measurements)){
                    for (var y = 0; y < results.length; y++){
                        var res = {};
                        var z = 0;
                        for (var x in results[y]){
                            if (x == "time"){
                                res[x] = results[y][x].valueOf();
                            } else {
                                res[measurements[z]] = results[y][x];
                                z++;
                            }
                        }
                        arr.push(res);
                    }
                } else {
                    for (var y = 0; y < results.length; y++){
                        var res = {};
                        for (var x in results[y]){
                            if (x == "device" || x == "provider"){
                                continue;
                            } else if (x == "time"){
                                res[x] = results[y][x].valueOf();
                            } else {
                                res[x.substring()] = results[y][x];
                            }
                        }
                        arr.push(res);
                    }
                }

                let start = new Date(from/1000000);
                
                if (typeof arr[0] != 'undefined'&& typeof arr[0].rain != 'undefined' && type == "time" && cumulative != 'd'){
                
                    if (cumulative == "m"){
                        start = new Date(start.getFullYear(), start.getMonth()).valueOf();
                    } else {
                        if (week == "week"){
                            start = new Date(start.getFullYear(), start.getMonth(),start.getDate()-start.getDay()).valueOf();
                        } else {
                            start = new Date(start.getFullYear(), start.getMonth(),start.getDate()-start.getDay()+1).valueOf();
                        }
                    }
                    start *= 1000000;
                    
                    backupQuery += "AND time >= " + start + " " + "AND time < " + from + " ";
                    backupQuery = backupQuery.replace("*", "sum(rain)");

                    influx.query(backupQuery).then(function(results){
                        if (results && typeof results[0] != 'undefinced' && typeof results[0].sum != 'undefinced'){
                            arr[0].rainC = results[0].sum;
                        }
                        callback(4, arr);
                    }).catch(function(err){
                        callback(4, arr);
                    })
                } else {
                    callback(4, arr);
                }
            }
        })
        .catch(function(err){
            callback(5);
        })
    }
}

module.exports = tsAPI;
