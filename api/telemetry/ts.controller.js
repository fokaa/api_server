/* 
 * InfluxDB telemetry APIs
 */

const router = require('express').Router();
const bodyParser = require('body-parser');

const tsAPI = require('./ts.api');
const midd = require('../../security/middleware')
const jwt = require('../auth/auth.jwt');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(jwt.passport.initialize());
router.use(jwt.passport.session());

router.get('/:id/last', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    tsAPI.getLastValues(req.user, req.params.id, query, function(reason, data){
       switch(reason){
            case 1:
               res.status(500).json("Unknown error");
               break;
            case 2:
               res.status(404).json("Device not found");
               break;
            case 3:
               res.status(401).json("Not authorized to access device");
               break;
           case 4:
               res.status(200).json(data);
               break;
            default:
               res.status(500).json("Unknown error");
        }
    })
});



router.get('/:id/:method', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    if (!req.params.method || (["min","max","mean","count","distinct","median","mode","spread","stddev","sum","integral"].indexOf(req.params.method.toLowerCase()) == -1)){
        res.status(400).json("Bad aggregation function");
        return;
    }
    if ((req.query) && (req.query.meas)){
        var query = req.query.meas.split(" ");
    }
    if ((req.query) && (req.query.sensors)){
        var query = req.query.sensors.split(" ");
    }
    tsAPI.getTimeSeries(req.user, req.params.id, req.query.from, req.query.to, req.query.agg, query, req.params.method, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized to access device");
                break;
            case 4:
                res.status(200).json(data);
                break;
            case 5:
                res.status(400).json("Bad query");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    });
});

//user, station, from, to, measurements, callback
router.get('/:id', jwt.passport.authenticate('jwt'), midd.robots, function(req, res, next){
    var rt = "time", rc = "d", w = "isoweek"
    if (req.query){
        if (req.query.meas){
            var query = req.query.meas.split(" ");
        }
        if (req.query.sensors){
            var query = req.query.sensors.split(" ");
        }
        if (req.query.rt){
            rt = req.query.rt;
        }
        if (req.query.rc){
            rc = req.query.rc;
        }
        if (req.query.w){
            w = req.query.w;
        }
    } 
    tsAPI.getTable(req.user, req.params.id, req.query.from, req.query.to, query, rt, rc, w, function(reason, data){
        switch(reason){
            case 1:
                res.status(500).json("Unknown error");
                break;
            case 2:
                res.status(404).json("Device not found");
                break;
            case 3:
                res.status(401).json("Not authorized to access device");
                break;
            case 4:
                res.status(200).json(data);
                break;
            case 5:
                res.status(400).json("Bad query");
                break;
            default:
                res.status(500).json("Unknown error");
        }
    });
});


module.exports = router;
