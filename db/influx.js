const Influx = require('influx');
const config = require('../config');

var server = {};
server.host = config.influx.host;
server.port = config.influx.port || "8086";

server.database = config.influx.database;
if (config.influx.user && config.influx.password){
    server.username = config.influx.user;
    server.password = config.influx.password;
}

const client = new Influx.InfluxDB(server);

client.getDatabaseNames()
    .then(function(names){
        if (!names.includes(config.influx.database)) {
            return influx.createDatabase(config.influx.database);
        }
    })
    .catch(function(err) {
        console.error("Error creating Influx database!");
        process.exit(1);
    });

module.exports = client;
