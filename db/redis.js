const redis = require('ioredis');
const config = require('../config.json');

var redisClient = new redis(config.redis.port, config.redis.host, {
    connectionName: "api"
})

module.exports = redisClient;