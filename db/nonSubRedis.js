const redis = require('ioredis');
const config = require('../config.json');

var redisClient = new redis(config.redis.port, config.redis.host, {
    connectionName: "apiNonSub"
})

module.exports = redisClient;